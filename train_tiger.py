import argparse
import time
import os


import magent.gridworld as gw
from magent.builtin.rule_model import RandomActor
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import EpisodesBuffer, linear_decay, sample_observation


def generate_map(env, handles):
    env.add_walls(method="random", n=50000)
    env.add_agents(handles[0],  method="random", n=50000)
    env.add_agents(handles[1], method="random", n=10000)


def play_a_round(env, handles, models, print_every, train_id=1, render=False, eps=None):
    env.reset()
    generate_map(env, handles)

    step_ct = 0
    total_reward = 0
    done = False

    n = len(handles)
    obs  = [[] for _ in range(n)]
    ids  = [[] for _ in range(n)]
    acts = [[] for _ in range(n)]
    nums = [0 for _ in range(n)]
    sample_buffer = EpisodesBuffer(10000)

    print("===== sample =====")
    print("eps %s" % eps)
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            acts[i] = models[i].infer_action(obs[i], ids[i], policy='e_greedy', eps=eps)
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        reward = 0
        if train_id != -1:
            rewards = env.get_reward(handles[train_id])
            alives  = env.get_alive(handles[train_id])
            total_reward += sum(rewards)
            sample_buffer.record_step(ids[train_id], obs[train_id], acts[train_id], rewards, alives)
            reward = sum(rewards)

        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        # stats info
        for i in range(n):
            nums[i] = env.get_num(handles[i])

        if step_ct % print_every == 0:
            print("step %3d,  deer: %5d,  tiger: %5d,  train_id: %d,  reward: %.2f,  total_reward: %.2f " %
                  (step_ct, nums[0], nums[1], train_id, reward, total_reward))
        step_ct += 1

        if step_ct > 10000:
            break

    sample_time = time.time() - start_time
    print("steps: %d, total time: %.2f, step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    total_loss = value = 0
    if train_id != -1:
        print("===== train =====")
        start_time = time.time()
        total_loss, value = models[train_id].train(sample_buffer)
        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)

    return total_loss, total_reward, value


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=2)
    parser.add_argument("--n_round", type=int, default=60000)
    parser.add_argument("--render", action="store_true")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--greedy", action="store_true")
    args = parser.parse_args()

    # init the game
    env = gw.GridWorld("forest", map_size=1000)
    env.set_render_dir("build/render")

    # two groups of animal
    deer_handle, tiger_handle = env.get_handles()

    # sample eval observation set
    print("sample eval set...")
    env.reset()
    generate_map(env, [deer_handle, tiger_handle])
    # eval_obs = sample_observation(env, [deer_handle, tiger_handle], 1, 2048)
    eval_obs = None

    # init two models
    models = [
        RandomActor(env, deer_handle, tiger_handle),
        RLModel(env, tiger_handle, "tiger", eval_obs=eval_obs)
    ]

    # load if
    savedir = 'save_model'
    start_from = args.load_from
    if start_from:
        print("load ... %d" % start_from)
        for model in models:
            model.load(savedir, start_from)
    else:
        start_from = 0

    # print debug info
    print(args)
    print("view_size", env.get_view_space(tiger_handle))
    print("learning rate", models[1].learning_rate)

    # play
    train_id = 1 if args.train else -1
    for k in range(start_from, start_from + args.n_round):
        eps = linear_decay(k, 4, 0.1) if not args.greedy else 0
        loss, deer_num, value = play_a_round(env, [deer_handle, tiger_handle], models,
                                             print_every=10, train_id=train_id, render=args.render,
                                             eps=eps)
        print("round %d, loss: %s, deer_num: %d, value: %.3f" % (k, loss, deer_num, value))

        if (k + 1) % args.save_every == 0:
            print("save model... ")
            if not os.path.exists(savedir):
                os.mkdir(savedir)
            for model in models:
                model.save(savedir, k)
