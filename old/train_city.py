import argparse
import logging as log
import math
import sys
import time
import os

import numpy as np
import tensorflow as tf

import magent.gridworld as gw
from magent.builtin.rule_model import RandomActor
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import EpisodesBuffer


def load_config(size):
    cfg = gw.Config()

    cfg.set({"map_width": size, "map_height": size})
    cfg.set({"turn_mode": 0})
    cfg.set({"minimap_mode": 1})

    cfg.set({"goal_mode": 1})

    cfg.set({"embedding_size": 16})

    small = cfg.add_agent_type(
        name="small",
        attr={'width': 1, 'length': 1, 'hp': 10, 'speed': 2,
              'view_range': gw.CircleRange(5), 'attack_range': gw.CircleRange(0)}
    )

    middle = cfg.add_agent_type(
        name="middle",
        attr={'width': 2, 'length': 2, 'hp': 10, 'speed': 1,
              'view_range': gw.CircleRange(5), 'attack_range': gw.CircleRange(0)}
    )

    big = cfg.add_agent_type(
        name="big",
        attr={'width': 2, 'length': 2, 'hp': 10, 'speed': 1,
              'view_range': gw.CircleRange(5), 'attack_range': gw.CircleRange(0)}
    )

    cfg.add_group(small)
    cfg.add_group(middle)

    return cfg


def generate_map(env, map_size, handles):
    # wall
    env.add_walls(method="random", n=map_size * map_size * 0.010)

    # agent
    env.add_agents(handles[0], method="random", n=map_size * map_size * 0.020)
    env.add_agents(handles[1], method="random", n=map_size * map_size * 0.008)
    #env.add_agents(handles[2], method="random", n=map_size * map_size * 0.008)

    # goals
    env.set_goal(handles[0], method="random")
    env.set_goal(handles[1], method="random")
    #env.set_goal(handles[2], method="random")


def play_a_round(env, map_size, handles, models, train=True, print_every=10, render=False, record=False,
                 eps=None):
    env.reset()
    generate_map(env, map_size, handles)

    step_ct = 0
    done = False

    n = len(handles)
    obs  = [None for _ in range(n)]
    ids  = [None for _ in range(n)]
    acts = [None for _ in range(n)]
    nums = [env.get_num(handle) for handle in handles]
    total_reward = [0 for _ in range(n)]
    sample_buffer = [EpisodesBuffer(5000) for _ in range(n)]

    print("===== sample =====")
    print("eps %s number %s" % (eps, nums))
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            acts[i] = models[i].infer_action(obs[i], ids[i], policy='e_greedy', eps=eps)
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        step_reward = []
        if train:
            for i in range(n):
                rewards = env.get_reward(handles[i])
                alives  = env.get_alive(handles[i])
                sample_buffer[i].sample_step(ids[i], obs[i], acts[i], rewards, alives)
                s = sum(rewards)
                step_reward.append(s)
                total_reward[i] += s
        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        if step_ct % print_every == 0:
            print("step %3d,  reward: %s,  total_reward: %s " %
                  (step_ct, np.around(step_reward, 2), np.around(total_reward, 2)))
        step_ct += 1

        if step_ct > 200:
            break

    sample_time = time.time() - start_time
    print("steps: %d,  total time: %.2f,  step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    total_loss, value = [0 for _ in range(n)], [0 for _ in range(n)]
    if train:
        print("===== train =====")
        start_time = time.time()
        for i in range(n):
            total_loss[i], value[i] = models[i].train(sample_buffer[i].buffer)
        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)

    round_list = lambda l: [round(x, 2) for x in l]
    return round_list(total_loss), round_list(total_reward), round_list(value)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=2)
    parser.add_argument("--n_round", type=int, default=60000)
    parser.add_argument("--render", action='store_true')
    parser.add_argument("--save_dir", type=str, default="save_model")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--print_every", type=int, default=100)
    parser.add_argument("--map_size", type=int, default=300)
    parser.add_argument("--greedy", action="store_true")
    args = parser.parse_args()

    # set logger
    log.basicConfig(level=log.INFO,
                    filename='city.log')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init env
    env = gw.GridWorld(load_config(size=args.map_size))
    env.set_render_dir("build/render")
    handles = env.get_handles()
    models  = []

    # init tensorflow
    config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    # load models
    models = [
        RLModel(env, handles[0], "small",  session=sess),
        RLModel(env, handles[1], "middle", session=sess),
        #RLModel(env, handles[2], "big",    session=sess),
    ]

    sess.run(tf.global_variables_initializer())

    # load saved model
    save_dir = args.save_dir
    if args.load_from is not None:
        print("load models...")
        start_from = args.load_from
        for model in models:
            model.load(save_dir, start_from)
    else:
        start_from = 0

    # print debug info
    print(args)
    print('view_size', env.get_view_space(handles[0]))
    print('feature_size', env.get_feature_space(handles[0]))
    print('action_space', env.get_action_space(handles[0]))

    # play
    for k in range(start_from, start_from + args.n_round):
        eps = max(0.10, 1 - k * 0.1) if not args.greedy else 0
        loss, reward, value = play_a_round(env, args.map_size, handles, models,
                                           print_every=20, train=args.train, render=args.render,
                                           eps=eps)  # for e-greedy
        log.info("round %d\t loss: %s\t reward: %s\t value: %s" % (k, loss, reward, value))

        if (k + 1) % args.save_every == 0 and args.train:
            print("save models...")
            if not os.path.exists(save_dir):
                os.mkdir(save_dir)
            for model in models:
                model.save(save_dir, k)
