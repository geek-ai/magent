import argparse
import logging as log
import math
import sys
import time
import os
import collections

import numpy as np
import tensorflow as tf

import magent.discrete_snake as ds
from magent.builtin.tf_model import DeepQNetwork
from magent.utility import EpisodesBuffer, piecewise_decay


def load_config(map_size):
    cfg = ds.Config()

    cfg.set({
        'map_width': map_size, 'map_height': map_size,
        'view_width': 11, 'view_height': 11,
        'embedding_size': 12,
        'max_dead_penalty': -5, 'corpse_value': 1,
        'total_resource': int(map_size * map_size * 0.06),
    })

    return cfg


def generate_map(map_size):
    env.reset()
    env.add_agent(method="random", n=map_size * map_size * 0.01)
    env.add_food(method="random", n=map_size * map_size * 0.01)


def play_steps(env, map_size, model, steps, replay_queue, train=True, print_every=25, render=False, record=False,
               eps=None):
    env.render_next_file()

    handle = 0

    num = env.get_num(handle)
    sample_buffer = EpisodesBuffer(20000)
    total_reward = 0

    const_num = map_size * map_size * 0.01

    # sample
    print("===== sample =====")
    print("eps %s number %s" % (eps, num))
    start_time = time.time()
    for i in range(steps):
        # take action
        obs = env.get_observation(handle)
        ids = env.get_agent_id(handle)
        acts = model.infer_action(obs, ids, policy='e_greedy', eps=eps)
        env.set_action(handle, acts)

        # step
        env.step()
        rewards = env.get_reward(handle)
        alives  = env.get_alive(handle)
        num     = env.get_num(handle)

        # sample
        if train:
            sample_buffer.sample_step(ids, obs, acts, rewards, alives)

        # render
        if render:
            env.render()

        env.clear_dead()

        # add new snakes
        env.add_agent(method="random", n=const_num - num)

        # stat info
        step_reward = sum(rewards)
        total_reward += step_reward
        lengths  = env.get_length(handle)
        average_length = 1.0 * sum(lengths) / num
        max_length = max(lengths)
        food_num = env.get_food_num()

        if i % print_every == 0:
            print("step: %3d\t food: %d\t ave_len: %.2f\t max_len: %.2f\t reward: %s\t total_reward: %s" %
                  (i, food_num, average_length, max_length, np.around(step_reward, 2),
                   np.around(total_reward, 2)))

    sample_time = time.time() - start_time
    print("steps: %d\t total_time: %.2f\t step average: %.2f" % (steps, sample_time, sample_time / steps))

    if record:
        to_sort = env.get_length(handle)
        to_sort = list(np.sort(to_sort)[::-1])
        with open("length-snake.txt", "a") as fout:
            fout.write(str(to_sort) + "\n")
    print(len(sample_buffer.get_dict_buf()))

    # train
    total_loss, value = 0, 0
    if train:
        print("===== train =====")
        start_time = time.time()
        replay_queue.append(sample_buffer)
        total_loss, value = model.train(replay_queue, print_every=500)
        train_time = time.time() - start_time
        print("train time %.2f" % train_time)

    return total_loss, total_reward, value, food_num, max_length

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=2)
    parser.add_argument("--n_round", type=int, default=2000)
    parser.add_argument("--step_per_round", type=int, default=180)
    parser.add_argument("--render", action='store_true')
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--print_every", type=int, default=50)
    parser.add_argument("--map_size", type=int, default=500)
    parser.add_argument("--greedy", action="store_true")
    parser.add_argument("--name", type=str, default="snake")
    parser.add_argument("--render_every", type=int, default=10)
    parser.add_argument("--record", action="store_true")
    args = parser.parse_args()

    # set logger
    log.basicConfig(level=log.INFO, filename=args.name + '.log')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init game
    env = ds.DiscreteSnake(load_config(args.map_size))
    env.set_render_dir("build/render")

    model = DeepQNetwork(env, 0, name="snake", train_freq=0.7)
    replay_queue = collections.deque(maxlen=3)

    # load saved model
    save_dir = "save_model"
    if args.load_from is not None:
        print("load models...")
        start_from = args.load_from
        model.load(save_dir, start_from)
    else:
        start_from = 0

    # print debug info
    print(args)
    print("view_space", env.get_view_space())
    print("feature_space", env.get_feature_space())
    print("action_space", env.get_action_space())

    # generate map
    generate_map(args.map_size)

    # play
    for k in range(start_from, start_from + args.n_round):
        start_time = time.time()
        eps = piecewise_decay(k, [0, 400, 600], [1, 0.1, 0.05]) if not args.greedy else 0

        loss, reward, value, food_num, max_length = \
            play_steps(env, args.map_size, model, args.step_per_round, replay_queue,
                       train=args.train, print_every=args.print_every,
                       render=args.render or (k+1) % args.render_every == 0,
                       record=args.record, eps=eps)
        log.info("round %d\t loss: %s\t reward: %s\t value: %s\t food: %s\t max_len: %s" %
                 (k, loss, reward, value, food_num, max_length))
        print("round time %.2f\n" % (time.time() - start_time))

        if (k + 1) % args.save_every == 0 and args.train:
            print("save models...")
            if not os.path.exists(save_dir):
                os.mkdir(save_dir)
            model.save(save_dir, k)