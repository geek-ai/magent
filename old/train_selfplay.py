import argparse
import logging
import math
import sys
import time
import os
import logging as log

import numpy as np
import tensorflow as tf

import magent.gridworld as gw
from magent.builtin.rule_model import RandomActor
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import EpisodesBuffer


def load_config(map_size):
    cfg = gw.Config()

    cfg.set({"map_width": map_size, "map_height": map_size})

    cfg.set({"turn_mode": 0})
    cfg.set({"minimap_mode": 1})

    small = cfg.add_agent_type(
        "small",
        {'width': 1, 'length': 1, 'hp': 5, 'speed': 2,
         'view_range': gw.CircleRange(4), 'attack_range': gw.CircleRange(1.5),
         'damage': 2, 'eat_ability': 0, 'step_recover': -0.008,
         'food_supply': 0,  'kill_supply': 0,
         'step_reward': 0,  'kill_reward': 1, 'dead_penalty': 0, 'attack_penalty': -0.2,
        })

    cfg.add_group(small)
    cfg.add_group(small)

    return cfg


def generate_map(env, map_size, handles):
    width = map_size
    height = map_size

    init_num = map_size * map_size * 0.04

    gap = 3
    # left
    n = init_num
    side = int(math.sqrt(n)) * 2
    pos = []
    for x in range(width/2 - gap - side, width/2 - gap - side + side, 2):
        for y in range((height - side)/2, (height - side)/2 + side, 2):
            pos.append([x, y, 0])
    env.add_agents(handles[0], method="custom", pos=pos)

    # right
    n = init_num
    side = int(math.sqrt(n)) * 2
    pos = []
    for x in range(width/2 + gap, width/2 + gap + side, 2):
        for y in range((height - side)/2, (height - side)/2 + side, 2):
            pos.append([x, y, 0])
    env.add_agents(handles[1], method="custom", pos=pos)


def play_a_round(env, map_size, handles, models, train_id, print_every=10,
                 render=False):
    env.reset()
    generate_map(env, map_size, handles)

    step_ct = 0
    total_reward = 0
    done = False

    n = len(handles)
    obs  = [None for _ in range(n)]
    ids  = [None for _ in range(n)]
    acts = [None for _ in range(n)]
    nums   = [0 for _ in range(n)]
    sample_buffer = EpisodesBuffer()

    print("===== sample =====")
    if train_id != -1:
        print(models[train_id].get_info())
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            acts[i] = models[i].infer_action(obs[i], ids[i], policy='e_greedy' if train_id != -1 else 'greedy')
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        if train_id != -1:
            reward = [None for _ in range(n)]
            for i in range(n):
                rewards = env.get_reward(handles[i])
                alives  = env.get_alive(handles[i])
                sample_buffer.sample_step(ids[i], obs[i], acts[i], rewards, alives)
                reward[i] = sum(rewards)
                total_reward += np.sum(np.where(rewards > 0, rewards, np.zeros_like(rewards)))
        else:
            reward = []

        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        # stats info
        for i in range(n):
            nums[i] = env.get_num(handles[i])

        if step_ct % print_every == 0:
            print("step %3d,  train %d,  num %s,  reward %s,  total_reward: %d" %
                  (step_ct, train_id, str(nums), str(reward), total_reward))
        step_ct += 1

    sample_time = time.time() - start_time
    print("steps: %d,  total time: %.2f,  step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    if train_id != -1:
        print("===== train =====")
        start_time = time.time()
        total_loss, value = models[train_id].train(sample_buffer.buffer, print_every=500)
        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)
    else:
        total_loss = value = 0

    return total_loss, total_reward, value

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=10)
    parser.add_argument("--n_round", type=int, default=60000)
    parser.add_argument("--render", action='store_true')
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--pretrain", action="store_true")
    parser.add_argument("--small", action="store_true")
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--print_every", type=int, default=50)
    parser.add_argument("--map_size", type=int, default=100)
    args = parser.parse_args()
    print(args)

    # set logger
    log.basicConfig(level=log.INFO,
                    filename='selfplay.log', filemode='w')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init env
    env = gw.GridWorld(load_config(args.map_size))
    env.set_render_dir("build/render")

    handles = env.get_handles()
    models  = []

    # int tensorflow
    config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    # load models
    models.append(RLModel(env, handles[0], "selfplay", explore_step=300000, session=sess))
    models.append(models[0])

    sess.run(tf.global_variables_initializer())

    # load if
    save_dir = "save_model"

    if args.load_from is not None:
        print("load models...")
        start_from = args.load_from
        for model in models:
            model.load(save_dir, start_from)
    else:
        start_from = 0

    # print debug info
    print(env.get_view_space(handles[0]))
    print(env.get_view2attack(handles[0]))

    # play
    train_id = 0 if args.train else -1
    for k in range(start_from, start_from + args.n_round):
        loss, reward, value = play_a_round(env, args.map_size, handles, models, train_id,
                                           print_every=args.print_every, render=args.render)
        log.info("round %d, loss: %.3f, reward: %d, value: %.3f" % (k, loss, reward, value))

        if args.train:
            if (k + 1) % args.save_every == 0:
                if not os.path.exists(save_dir):
                    os.mkdir(save_dir)
                print("save models...")
                for model in models:
                    model.save(save_dir, k)

