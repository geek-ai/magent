import argparse
import logging as log
import math
import sys
import time
import os

import numpy as np
import tensorflow as tf

import magent.gridworld as gw
from magent.builtin.rule_model import RandomActor
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import EpisodesBuffer

MAX_RADIUS = 4


def load_config(size):
    cfg = gw.Config()

    cfg.set({"map_width": size, "map_height": size})
    cfg.set({"turn_mode": 0})
    cfg.set({"minimap_mode": 1})

    cfg.set({"embedding_size": 16})

    small = cfg.add_agent_type(
        name="small",
        attr={'width': 1, 'length': 1, 'hp': 10, 'speed': 2,
              'view_range': gw.CircleRange(MAX_RADIUS), 'attack_range': gw.CircleRange(1.5),
              'damage': 4, 'step_recover': 0.05,
              'kill_supply': 0,
              'step_reward': 0,  'kill_reward': 1, 'dead_penalty': -1, 'attack_penalty': -0.1,
              'attack_in_group': 1})

    cfg.add_group(small)

    return cfg


def generate_map(env, map_size, handles):
    # agent
    env.add_agents(handles[0], method="random", n=map_size * map_size * 0.020)


def build_mask(max_radius, n_channel):
    width = max_radius * 2 + 1
    masks = np.zeros(shape=[max_radius, width, width, n_channel])
    for k in range(max_radius):
        r = k + 1
        for row in range(max_radius - r, max_radius + r + 1):
            for col in range(max_radius - r, max_radius + r + 1):
                masks[k, row, col, :] = 1
    return masks


def get_level(reward):
    level = max(0, min(int(reward/2), MAX_RADIUS-1))
    return level


def play_a_round(env, map_size, handles, models, train_id, print_every=10, render=False, record=False, eps=None):
    env.reset()
    generate_map(env, map_size, handles)

    step_ct = 0
    total_reward = 0
    done = False

    n = len(handles)
    obs  = [None for _ in range(n)]
    ids  = [None for _ in range(n)]
    acts = [None for _ in range(n)]
    nums = [env.get_num(handle) for handle in handles]
    sample_buffer = EpisodesBuffer(capacity=20000)
    id_reward = {}
    dead_ct = 0

    masks = build_mask(MAX_RADIUS, 4)

    all_true = np.empty(nums[0])
    all_true[:] = True

    print("===== sample =====")
    print("eps %s number %s" % (eps, nums))
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            # mask observation
            for j in xrange(len(ids[i])):
                level = get_level(id_reward.get(ids[i][j], 0))
                obs[i][0][j] *= masks[level]

            acts[i] = models[i].infer_action(obs[i], ids[i], policy='e_greedy', eps=eps)
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        rewards = env.get_reward(handles[0])
        reward = sum(rewards)
        if train_id != -1:
            alives  = env.get_alive(handles[train_id])
            total_reward += sum(rewards)
            sample_buffer.sample_step(ids[train_id], obs[train_id], acts[train_id], rewards, alives)
        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        for id, r in zip(ids[train_id], rewards):
            if id not in id_reward:
                id_reward[id] = 0
            id_reward[id] += r

        dead_id = ids[train_id][~alives]
        for id in dead_id:
            del id_reward[id]
        dead_ct += len(dead_id)
        env.add_agents(handles[train_id], method="random", n=len(dead_id))

        # stats info
        for i in range(n):
            nums[i] = env.get_num(handles[i])

        if step_ct % print_every == 0:
            print("step %3d  train %d  num %s  reward %.2f,  total_reward: %.2f dead_ct: %d" %
                  (step_ct, train_id, nums, reward, total_reward, dead_ct))
        step_ct += 1

        if step_ct > 200:
            break

    to_sort = []
    for id, r in id_reward.iteritems():
        to_sort.append(r)
    to_sort.sort(reverse=True)
    print(to_sort[:10])

    if record:
        with open("reward-evolve.txt", "a") as fout:
            fout.write(str(to_sort) + "\n")

    sample_time = time.time() - start_time
    print("steps: %d,  total time: %.2f,  step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    total_loss = value = 0
    if train_id != -1:
        print("===== train =====")
        print("len", len(sample_buffer.buffer))
        start_time = time.time()
        total_loss, value = models[train_id].train(sample_buffer.buffer, print_every=500)
        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)

    return total_loss, total_reward, value


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=2)
    parser.add_argument("--n_round", type=int, default=60000)
    parser.add_argument("--render", action='store_true')
    parser.add_argument("--save_dir", type=str, default="save_model")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--print_every", type=int, default=50)
    parser.add_argument("--map_size", type=int, default=500)
    parser.add_argument("--record", action="store_true")
    parser.add_argument("--greedy", action="store_true")
    args = parser.parse_args()

    # set logger
    log.basicConfig(level=log.INFO,
                    filename='evolve.log')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init env
    env = gw.GridWorld(load_config(size=args.map_size))
    env.set_render_dir("build/render")
    handles = env.get_handles()
    models = []

    # load models
    models.append(RLModel(env, handles[0], "evolve"))

    # load saved model
    save_dir = args.save_dir
    if args.load_from is not None:
        print("load models...")
        start_from = args.load_from
        for model in models:
            model.load(save_dir, start_from)
    else:
        start_from = 0

    # print debug info
    print(args)
    print(env.get_view_space(handles[0]))
    print(env.get_view2attack(handles[0]))

    # sample data
    if args.record:
        for k in range(9, 400, 10):
            for model in models:
                model.load(save_dir, k)
            play_a_round(env, args.map_size, handles, models, -1,
                         print_every=args.print_every, record=True, eps=0)
    # play
    else:
        train_id = 0 if args.train else -1
        for k in range(start_from, start_from + args.n_round):
            eps = max(0.10, 1 - k * 0.1) if not args.greedy else 0
            loss, reward, value = play_a_round(env, args.map_size, handles,
                                               models, train_id,
                                               print_every=args.print_every, render=args.render,
                                               eps=eps)
            log.info("round %d\t loss: %.3f\t reward: %.2f\t value: %.3f" % (k, loss, reward, value))

            if (k + 1) % args.save_every == 0 and args.train:
                if not os.path.exists(save_dir):
                    os.mkdir(save_dir)
                print("save models...")
                for model in models:
                    model.save(save_dir, k)
