import argparse
import time
import os
import collections
import logging as log

import numpy as np

import magent.gridworld as gw
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue, piecewise_decay


def load_config(map_size):
    cfg = gw.Config()

    cfg.set({"map_width": map_size, "map_height": map_size})
    cfg.set({"embedding_size": 16})

    predator = cfg.add_agent_type(
        "predator",
        {'width': 2, 'length': 2, 'hp': 1, 'speed': 1,
         'view_range': gw.CircleRange(5), 'attack_range': gw.CircleRange(2),
         'step_reward': 0, 'attack_penalty': -0.2})

    prey = cfg.add_agent_type(
        "prey",
        {'width': 1, 'length': 1, 'hp': 1, 'speed': 1.5,
         'view_range': gw.CircleRange(4), 'attack_range': gw.CircleRange(0),
         'step_reward': 0})

    predator_group  = cfg.add_group(predator)
    prey_group = cfg.add_group(prey)

    a = gw.AgentSymbol(predator_group, index='any')
    b = gw.AgentSymbol(prey_group, index='any')

    cfg.add_reward_rule(gw.Event(a, 'attack', b), receiver=[a, b], value=[1, -1])

    return cfg


def play_a_round(env, map_size, handles, models, replay_queues, print_every, train=True, render=False, eps=None):
    env.reset()

    env.add_walls(method="random", n=map_size * map_size * 0.03)
    env.add_agents(handles[0], method="random", n=map_size * map_size * 0.0125)
    env.add_agents(handles[1], method="random", n=map_size * map_size * 0.025)

    step_ct = 0
    done = False

    n = len(handles)
    nums = [env.get_num(handle) for handle in handles]
    total_reward = [0 for _ in range(n)]
    sample_buffer = [EpisodesBuffer(5000) for _ in range(n)]

    print("===== sample =====")
    print("eps %s number %s" % (eps, nums))
    start_time = time.time()
    while not done:
        # take actions for every model
        obs = [env.get_observation(handle) for handle in handles]
        ids = [env.get_agent_id(handle) for handle in handles]
        acts = [models[i].infer_action(obs[i], ids[i], policy='e_greedy', eps=eps) for i in range(n)]

        for i in range(n):
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        step_reward = []
        for i in range(n):
            rewards = env.get_reward(handles[i])
            if train:
                alives  = env.get_alive(handles[i])
                sample_buffer[i].sample_step(ids[i], obs[i], acts[i], rewards, alives)
            s = sum(rewards)
            step_reward.append(s)
            total_reward[i] += s

        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        if step_ct % print_every == 0:
            print("step %3d,  reward: %s,  total_reward: %s " %
                  (step_ct, np.around(step_reward, 2), np.around(total_reward, 2)))
        step_ct += 1

        if step_ct > 225:
            break

    sample_time = time.time() - start_time
    print("steps: %d,  total time: %.2f,  step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    total_loss, value = [0 for _ in range(n)], [0 for _ in range(n)]
    if train:
        print("===== train =====")
        start_time = time.time()
        for i in range(n):
            replay_queues[i].append(sample_buffer[i])
            total_loss[i], value[i] = models[i].train(replay_queues[i], print_every=2000)

        train_time = time.time() - start_time
        print("train time %.2f" % train_time)

    def round_list(l): return [round(x, 2) for x in l]
    return round_list(total_loss), round_list(total_reward), round_list(value)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=2)
    parser.add_argument("--render_every", type=int, default=10)
    parser.add_argument("--n_round", type=int, default=500)
    parser.add_argument("--render", action="store_true")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--map_size", type=int, default=800)
    parser.add_argument("--greedy", action="store_true")
    parser.add_argument("--name", type=str, default="pursuit")
    args = parser.parse_args()

    # set logger
    log.basicConfig(level=log.INFO, filename=args.name + '.log')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init game
    env = gw.GridWorld(load_config(args.map_size))
    env.set_render_dir("build/render")

    # two groups of animal
    handles = env.get_handles()

    # init two models
    models = [
        RLModel(env, handles[0], args.name + "-predator", train_freq=1, batch_size=512),
        RLModel(env, handles[1], args.name + "-prey", train_freq=1, batch_size=512)
    ]
    replay_queues = [EpisodesBuffersQueue(len=3) for _ in range(len(models))]

    # load if
    savedir = 'save_model'
    start_from = args.load_from
    if start_from:
        print("load ... %d" % start_from)
        for model in models:
            model.load(savedir, start_from)
    else:
        start_from = 0

    # print debug info
    print(args)
    print("view_space", env.get_view_space(handles[0]))
    print("feature_space", env.get_feature_space(handles[0]))

    # play
    for k in range(start_from, start_from + args.n_round):
        start_time = time.time()
        eps = piecewise_decay(k, [0, 200, 400], [1, 0.1, 0.05]) if not args.greedy else 0

        loss, reward, value = play_a_round(env, args.map_size, handles, models, replay_queues,
                                           print_every=50, train=args.train,
                                           render=args.render or (k+1) % args.render_every == 0,
                                           eps=eps)  # for e-greedy
        log.info("round %d\t loss: %s\t reward: %s\t value: %s" % (k, loss, reward, value))
        print("round time %.2f\n" % (time.time() - start_time))

        if (k + 1) % args.save_every == 0 and args.train:
            print("save model... ")
            if not os.path.exists(savedir):
                os.mkdir(savedir)
            for model in models:
                model.save(savedir, k)
