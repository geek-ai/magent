import time
import os

import numpy as np
import tensorflow as tf


class DeepQNetwork:
    EXPLORE_STEP     = 5000
    INITIAL_RANDOM_PROB = 1.0
    FINAL_RANDOM_PROB   = 0.1

    TARGET_UPDATE_FREQ = 400
    TRAIN_FREQ = 1

    def __init__(self, env, handle, name, agent_emb_dim=0,
                 stack_frame=2, batch_size=16, reward_decay=0.99, learning_rate=1e-5,
                 session=None, use_dueling=True, use_double=False, use_terminal=True,
                 use_prior_sample=False):
        # ======================== set config  ========================
        self.env = env
        self.handle = handle
        self.name = name
        self.agent_emb_dim = agent_emb_dim
        self.view_size = env.get_view_space(handle)
        self.num_actions  = env.get_action_space(handle)
        self.feature_size = agent_emb_dim + 1  # + hp
        self.batch_size   = batch_size
        self.stack_frame  = stack_frame

        self.use_dueling  = use_dueling
        self.use_double   = use_double
        self.use_terminal = use_terminal
        self.use_prior_sample = use_prior_sample

        self.train_ct = 0

        # ======================= build network =======================
        # input place holder
        self.target  = tf.placeholder(tf.float32, [None])

        self.input_view    = tf.placeholder(tf.float32, [None, self.view_size[0], self.view_size[1],
                                                               self.view_size[2] * stack_frame])
        self.input_feature = tf.placeholder(tf.float32, [None, self.feature_size * self.stack_frame])
        self.action = tf.placeholder(tf.int32, [None])

        # build graph
        with tf.variable_scope(self.name):
            with tf.variable_scope("eval_net_scope"):
                self.eval_scope_name   = tf.get_variable_scope().name
                self.qvalues = self._create_network(self.input_view, self.input_feature,
                                                    self.view_size, self.feature_size, self.stack_frame)

            with tf.variable_scope("target_net_scope"):
                self.target_scope_name = tf.get_variable_scope().name
                self.target_qvalues = self._create_network(self.input_view, self.input_feature,
                                                           self.view_size, self.feature_size, self.stack_frame)

        # train op
        self.gamma = reward_decay
        self.actions_onehot = tf.one_hot(self.action, self.num_actions)
        self.loss = tf.reduce_mean(
            tf.square(
                self.target - tf.reduce_sum(tf.multiply(self.actions_onehot, self.qvalues), axis=1)
            ))

        self.train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.loss)

        # target network update op
        self.update_target_op = []
        t_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.target_scope_name)
        e_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.eval_scope_name)
        for i in range(len(t_params)):
            self.update_target_op.append(tf.assign(t_params[i], e_params[i]))

        # init tensorflow session
        if session is None:
            config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
            config.gpu_options.allow_growth = True
            self.sess = tf.Session(config=config)
            self.sess.run(tf.global_variables_initializer())
        else:
            self.sess = session

    def _create_network(self, input_view, input_feature, view_size, feature_size, stack_frame,
                        use_conv=False):
        def weight_variable(shape):
            initial = tf.truncated_normal(shape, stddev=0.1)
            return tf.Variable(initial)

        def bias_variable(shape):
            initial = tf.constant(0.1, shape=shape)
            return tf.Variable(initial)

        def conv2d(x, W):
            return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

        def max_pool_2x2(x):
            return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                                  padding='SAME')

        hidden_size = [256, 128]

        if use_conv:
            n_channel = view_size[2]
            flat_size = ((view_size[0] + 3) / 4) * ((view_size[1] + 3) / 4)

            W_conv1 = weight_variable([4, 4, n_channel, 32])
            b_conv1 = bias_variable([32])
            h_conv1 = tf.nn.relu(conv2d(input_view, W_conv1) + b_conv1)
            h_pool1 = max_pool_2x2(h_conv1)

            W_conv2 = weight_variable([2, 2, 32, 64])
            b_conv2 = bias_variable([64])
            h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
            h_pool2 = max_pool_2x2(h_conv2)

            flatten = tf.reshape(h_pool2, [-1, flat_size * 64])
        else:
            flat_size = view_size[0] * view_size[1] * view_size[2] * stack_frame
            flatten = tf.reshape(input_view, [-1, flat_size])

            # a fully connected layer
            if hidden_size[0] > 0:
                W_fc = weight_variable([flat_size, hidden_size[0]])
                b_fc = bias_variable([hidden_size[0]])
                flatten = tf.nn.relu(tf.matmul(flatten, W_fc) + b_fc)

        input_feature = tf.reshape(input_feature, [-1, feature_size * stack_frame])

        all_flat = tf.concat((flatten, input_feature), axis=1)
        flatten_size = int(str(all_flat.shape[1]))

        W_dense   = weight_variable([flatten_size, hidden_size[1]])
        b_dense   = bias_variable([hidden_size[1]])
        h_dense   = tf.nn.relu(tf.matmul(all_flat, W_dense) + b_dense)

        if self.use_dueling:
            # state value
            W_v = weight_variable([hidden_size[1], 1])
            b_v = bias_variable([1])
            value = tf.matmul(h_dense, W_v) + b_v

            # advantage
            W_a = weight_variable([hidden_size[1], self.num_actions])
            b_a = bias_variable([self.num_actions])
            advantage = tf.matmul(h_dense, W_a) + b_a

            qvalues = value + advantage - tf.reduce_mean(advantage, axis=1, keep_dims=True)
        else:
            W_out   = weight_variable([hidden_size[1], self.num_actions])
            b_out   = bias_variable([self.num_actions])
            qvalues = tf.matmul(h_dense, W_out) + b_out

        return qvalues

    def _process_with_emb(self, id, raw_obs):
        l = len(raw_obs[0])
        view_ = np.empty((l,) + raw_obs[0][0].shape)
        feature_ = np.empty((l, ) + raw_obs[1][0].shape)
        for i in xrange(len(raw_obs[0])):
            view_[i] = raw_obs[0][i]
            feature_[i] = raw_obs[1][i]

        return view_, feature_

    def infer_action(self, raw_obs, ids, policy='e_greedy', eps=None):
        view, feature = self._process_with_emb(ids, raw_obs)
        n = len(ids)

        qvalues = self.sess.run(self.qvalues, {self.input_view: view,
                                               self.input_feature: feature})
        best_actions = np.argmax(qvalues, axis=1)

        if policy == 'e_greedy':
            if eps is None:
                eps = max(self.INITIAL_RANDOM_PROB - self.train_ct * (self.INITIAL_RANDOM_PROB - self.FINAL_RANDOM_PROB) /
                          self.EXPLORE_STEP, self.FINAL_RANDOM_PROB)
            random = np.random.randint(self.num_actions, size=(n,))
            cond = np.random.uniform(0, 1, size=(n,)) < eps
            ret = np.where(cond, random, best_actions)
        elif policy == 'greedy':
            ret = best_actions

        return ret.astype(np.int32)

    def _calc_target(self, next_view, next_feature, rewards, terminal):
        t_qvalues = self.sess.run(self.target_qvalues, {self.input_view: next_view,
                                                        self.input_feature: next_feature})
        n = len(rewards)
        if self.use_double:
            qvalues = self.sess.run(self.qvalues, {self.input_view: next_view,
                                                   self.input_feature: next_feature})
            next_value = t_qvalues[np.arange(n), np.argmax(qvalues, axis=1)]
        else:
            next_value = np.max(t_qvalues, axis=1)

        if self.use_terminal:
            target = np.where(terminal, rewards, rewards + self.gamma * next_value)
        else:
            target = rewards + self.gamma * next_value

        return target

    def train(self, sample_buffer, print_every=1000):
        batch_size = self.batch_size
        stack_frame = self.stack_frame
        total_loss = 0

        n = 0
        for episode in sample_buffer.values():
            n += len(episode.rewards)
        n_head = n
        n = n + len(sample_buffer) * (stack_frame - 1) + 1  # add pad

        # collect episode from dict to a single buffer
        view = np.empty((n,) + self.view_size, dtype=np.float32)
        feature = np.empty((n, self.feature_size), dtype=np.float32)
        action, reward = np.empty(n, dtype=np.int32), np.empty(n, dtype=np.float32)
        terminal = np.empty(n, dtype=np.bool)
        head = np.empty(n_head, dtype=np.int32)

        ct, p_head = 0, 0
        for id, episode in sample_buffer.iteritems():
            v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards
            m = len(episode.rewards)

            for i in range(stack_frame - 1):
                view[ct] = v[0]
                feature[ct] = f[0]
                ct += 1
            head[p_head:p_head + m] = np.arange(ct, ct + m)
            p_head += m

            for i in range(m):
                view[ct] = v[i]            # s  (view + feature)
                feature[ct] = f[i]
                action[ct] = a[i]          # a
                reward[ct] = r[i]          # r
                terminal[ct] = (i == m - 1)

                ct += 1
        # pad final
        view[ct] = view[ct-1]
        feature[ct] = feature[ct-1]
        ct += 1
        assert n == ct and n_head == p_head

        # for shuffle
        shuffled = head[np.random.permutation(n_head)]

        # train batches
        train_round = int(self.TRAIN_FREQ * n_head / batch_size)

        batch_view = np.empty((batch_size, self.view_size[0], self.view_size[1], self.view_size[2] * stack_frame), dtype=np.float32)
        batch_feature = np.empty((batch_size, self.feature_size * stack_frame), dtype=np.float32)
        batch_next_view = np.empty_like(batch_view)
        batch_next_feature = np.empty_like(batch_feature)
        n_channel = self.view_size[2]
        feature_size = self.feature_size

        ct = 0
        print("batches %d" % train_round)
        start_time = time.time()
        mean_qvalue = 0
        for i in range(train_round):
            pick = shuffled[i * batch_size:(i+1) * batch_size]
            for j, start in enumerate(pick):
                for k in range(stack_frame):
                    batch_view[j, ..., k * n_channel:(k+1) * n_channel] \
                        = view[start - stack_frame + 1 + k]
                    batch_feature[j, k * feature_size:(k+1) * feature_size] \
                        = feature[start - stack_frame + 1 + k]

                    batch_next_view[j, ..., k * n_channel:(k+1) * n_channel] \
                        = view[start - stack_frame + 2 + k]
                    batch_next_feature[j, k * feature_size:(k+1) * feature_size] \
                        = feature[start - stack_frame + 2 + k]

            target = self._calc_target(batch_next_view, batch_next_feature,
                                       reward[pick], terminal[pick])
            _, loss = self.sess.run([self.train_op, self.loss], feed_dict={
                self.input_view:    batch_view,
                self.input_feature: batch_feature,
                self.action:        action[pick],
                self.target:        target,
            })
            total_loss += loss

            if self.train_ct % self.TARGET_UPDATE_FREQ == 0:
                self.sess.run(self.update_target_op)

            mean_qvalue = 0.98 * mean_qvalue + np.mean(target) * 0.02
            if ct % print_every == 0:
                print("batch %5d | loss %.6f qvalue %.6f" % (ct, loss, mean_qvalue))

            ct += 1
            self.train_ct += 1
        total_time = time.time() - start_time
        print("batches: %d, total time: %.2f  step average: %.2f" % (ct, total_time, total_time / (ct * print_every)))

        return total_loss / ct if ct != 0 else 0, mean_qvalue

    def save(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, "tfdqn_%d" % epoch)
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.save(self.sess, os.path.join(dir_name, self.name))

    def load(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, "tfdqn_%d" % epoch)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.restore(self.sess, os.path.join(dir_name, self.name))

        self.train_ct = self.EXPLORE_STEP / 2

    def get_info(self):
        eps = max(self.INITIAL_RANDOM_PROB - self.train_ct * (self.INITIAL_RANDOM_PROB - self.FINAL_RANDOM_PROB) /
                  self.EXPLORE_STEP, self.FINAL_RANDOM_PROB)
        return "eps: %g  train_time: %d" % (eps, self.train_ct)
