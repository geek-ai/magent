import argparse
import time
import os

import numpy as np

import magent.gridworld as gw
from magent.builtin.rule_model import RandomActor
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import EpisodesBuffer


def load_config_live():
    cfg = gw.Config()

    cfg.set({"map_width": 1000, "map_height": 1000})

    deer = cfg.add_agent_type(
        "deer",
        {'width': 1, 'length': 1, 'hp': 1, 'speed': 1,
         'view_range': gw.CircleRange(4), 'attack_range': gw.CircleRange(0),
         'damage': 0, 'eat_ability': 0, 'step_recover': 0,
         'food_supply': 0, 'kill_supply': 8,
         'step_reward': 0, 'kill_reward': 0, 'dead_penalty': -1})

    tiger = cfg.add_agent_type(
        "tiger",
        {'width': 1, 'length': 1, 'hp': 10, 'speed': 1,
         'view_range': gw.CircleRange(4), 'attack_range': gw.SectorRange(1, 120),
         'damage': 3, 'eat_ability': 8, 'step_recover': -0.5,
         'food_supply': 0, 'kill_supply': 0,
         'step_reward': 1, 'kill_reward': 0, 'dead_penalty': 0})

    cfg.add_group(deer)
    cfg.add_group(tiger)

    return cfg


def load_config_kill():
    cfg = gw.Config()

    cfg.set({"map_width": 1000, "map_height": 1000})
    cfg.set({"food_mode": 0})

    deer = cfg.add_agent_type(
                "deer",
                {'width': 1, 'length': 1, 'hp': 1, 'speed': 1,
                 'view_range': gw.CircleRange(4), 'attack_range': gw.CircleRange(0),
                 'damage': 0, 'step_recover': 0,    'step_reward': 0,
                 'food_supply': 0, 'kill_supply': 8,
                 })

    tiger = cfg.add_agent_type(
                "tiger",
                {'width': 1, 'length': 1, 'hp': 10, 'speed': 1,
                 'view_range': gw.CircleRange(4), 'attack_range': gw.SectorRange(1, 120),
                 'damage': 3, 'step_recover': -0.5, 'step_reward': 0,
                 'food_supply': 0, 'kill_supply': 0, 'attack_penalty': -0.1,
                 'attack_in_group': 1})

    deer_group  = cfg.add_group(deer)
    tiger_group = cfg.add_group(tiger)

    d = gw.AgentSymbol(deer_group, index='any')
    t = gw.AgentSymbol(tiger_group, index='any')

    cfg.add_reward_rule(gw.Event(t, 'kill', d), receiver=t, value=1)

    return cfg


class StackFrameProcessor:
    def __init__(self, stack_frame=2):
        self.buffer = {}
        self.stack_frame = stack_frame

    def process(self, ids, obs):
        shape = obs[0].shape
        n_channel = shape[-1]
        stack_frame = self.stack_frame
        new_shape = list((len(ids),) + obs[0].shape)
        new_shape[-1] *= stack_frame
        ret = np.empty(new_shape, dtype=np.float32)
        for ii in xrange(len(ids)):
            copy = obs[ii].copy()

            if ids[ii] not in self.buffer:
                buf = [copy, copy, copy]
                self.buffer[ids[ii]] = buf
            else:
                buf = self.buffer[ids[ii]]

            for j in range(stack_frame - 1):
                ret[ii, ..., j * n_channel:(j+1) * n_channel] = buf[j]
            ret[ii, ..., (stack_frame-1) * n_channel:stack_frame * n_channel] = copy

            buf[0:stack_frame-2] = buf[1:stack_frame-1]
            buf[stack_frame-2] = copy

        return ret


def play_a_round(env, handles, models, print_every, train_id=1, render=False):
    env.reset()

    env.add_walls(method="random", n=50000)
    env.add_agents(deer_handle,  method="random", n=50000)
    env.add_agents(tiger_handle, method="random", n=10000)

    step_ct = 0
    total_reward = 0
    done = False

    n = len(handles)
    obs  = [[] for _ in range(n)]
    ids  = [[] for _ in range(n)]
    acts = [[] for _ in range(n)]
    nums   = [0 for _ in range(n)]

    view_processor = StackFrameProcessor()
    feature_processor = StackFrameProcessor()
    sample_buffer = EpisodesBuffer()

    print("===== sample =====")
    if train_id != -1:
        print(models[train_id].get_info())
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            stacked_obs = view_processor.process(ids[i], obs[i][0]), feature_processor.process(ids[i], obs[i][1])
            acts[i] = models[i].infer_action(stacked_obs, ids[i], policy='e_greedy' if train_id == i else 'greedy')
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        if train_id != -1:
            rewards = env.get_reward(handles[train_id])
            total_reward += sum(rewards)
            sample_buffer.sample_step(ids[train_id], obs[train_id], acts[train_id], rewards)
            reward = sum(rewards)
        else:
            reward = 0

        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        # stats info
        for i in range(n):
            nums[i] = env.get_num(handles[i])

        if step_ct % print_every == 0:
            print("step %3d | deer: %5d  tiger: %5d  train_id: %d  reward: %4d  total_reward: %d " %
                  (step_ct, nums[0], nums[1], train_id, reward, total_reward))
        step_ct += 1

    sample_time = time.time() - start_time
    print("steps: %d, total time: %.2f, step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    if train_id != -1:
        print("===== train =====")
        total_loss = 0
        start_time = time.time()
        models[train_id].train(sample_buffer.buffer)
        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)
    else:
        total_loss = 0

    return total_loss, total_reward


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=1)
    parser.add_argument("--n_round", type=int, default=60000)
    parser.add_argument("--render", action="store_true")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    args = parser.parse_args()
    print(args)

    # init the game
    env = gw.GridWorld(load_config_kill())
    env.set_render_dir("build/render")

    # two groups of animal
    deer_handle, tiger_handle = env.get_handles()
    print("view_size", env.get_view_space(tiger_handle))

    # init two models
    models = [
        RandomActor(env, deer_handle, tiger_handle),
        RLModel(env, tiger_handle, "tiger")
    ]

    # load if
    savedir = 'save_model'
    start_from = args.load_from
    if start_from:
        print("load ... %d" % start_from)
        for model in models:
            model.load(savedir, start_from)
    else:
        start_from = 0

    # play
    train_id = 1 if args.train else -1
    for i in range(start_from, start_from + args.n_round):
        loss, deer_num = play_a_round(env, [deer_handle, tiger_handle], models,
                                      print_every=10, train_id=train_id,
                                      render=args.render)
        print("round %d  loss: %.3f  reward: %d\n" % (i, loss, deer_num))

        if (i + 1) % args.save_every == 0:
            print("save model... ")
            if not os.path.exists(savedir):
                os.mkdir(savedir)
            for model in models:
                model.save(savedir, i)
