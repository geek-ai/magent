import argparse
import time
import os
import logging as log
import math

import numpy as np
import random

import magent.gridworld as gw
from magent.builtin.tf_model import DeepRecurrentQNetwork as RLModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue


def load_config(map_size):
    cfg = gw.Config()

    cfg.set({"map_width": map_size, "map_height": map_size})
    cfg.set({"minimap_mode": True})

    cfg.set({"embedding_size": 10})

    small = cfg.add_agent_type(
        "small",
        {'width': 1, 'length': 1, 'hp': 10, 'speed': 2,
         'view_range': gw.CircleRange(6), 'attack_range': gw.CircleRange(1.5),
         'damage': 2, 'step_recover': 0.1,
         'step_reward': -0.001,  'kill_reward': 100, 'dead_penalty': -0.05, 'attack_penalty': -1,
         })

    g0 = cfg.add_group(small)
    g1 = cfg.add_group(small)

    a = gw.AgentSymbol(g0, index='any')
    b = gw.AgentSymbol(g1, index='any')

    cfg.add_reward_rule(gw.Event(a, 'attack', b), receiver=a, value=2)
    cfg.add_reward_rule(gw.Event(b, 'attack', a), receiver=b, value=2)

    return cfg


def generate_map(env, map_size, handles):
    width = map_size
    height = map_size

    init_num = map_size * map_size * 0.04

    gap = 3
    leftID, rightID = 0, 1
    if random.randint(0, 1) == 1:
        leftID, rightID = 1, 0
    # left
    n = init_num
    side = int(math.sqrt(n)) * 2
    pos = []
    for x in range(width/2 - gap - side, width/2 - gap - side + side, 2):
        for y in range((height - side)/2, (height - side)/2 + side, 2):
            pos.append([x, y, 0])  # x, y, dir
    random.shuffle(pos)
    env.add_agents(handles[leftID], method="custom", pos=pos)

    # right
    n = init_num
    side = int(math.sqrt(n)) * 2
    pos = []
    for x in range(width/2 + gap, width/2 + gap + side, 2):
        for y in range((height - side)/2, (height - side)/2 + side, 2):
            pos.append([x, y, 0])  # x, y, dir
    random.shuffle(pos)
    env.add_agents(handles[rightID], method="custom", pos=pos)


def play_a_round(env, map_size, handles, models, replay_queues, print_every, train=True, render=False, eps=None):
    env.reset()
    generate_map(env, map_size, handles)

    step_ct = 0
    done = False

    n = len(handles)
    obs  = [[] for _ in range(n)]
    ids  = [[] for _ in range(n)]
    acts = [[] for _ in range(n)]
    nums = [env.get_num(handle) for handle in handles]
    total_reward = [0 for _ in range(n)]
    sample_buffer = [EpisodesBuffer(5000) for _ in range(n)]

    print("===== sample =====")
    print("eps %.2f number %s" % (eps, nums))
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            acts[i] = models[i].infer_action(obs[i], ids[i], policy='e_greedy', eps=eps)
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        step_reward = []
        for i in range(n):
            rewards = env.get_reward(handles[i])
            if train:
                alives  = env.get_alive(handles[i])
                sample_buffer[i].sample_step(ids[i], obs[i], acts[i], rewards, alives)
            s = sum(rewards)
            step_reward.append(s)
            total_reward[i] += s

        # render
        if render:
            env.render()

        # stat info
        nums = [env.get_num(handle) for handle in handles]

        # clear dead agents
        env.clear_dead()

        if step_ct % print_every == 0:
            print("step %3d,  nums: %s reward: %s,  total_reward: %s " %
                  (step_ct, nums, np.around(step_reward, 2), np.around(total_reward, 2)))
        step_ct += 1
        
        if step_ct > 500:
            break

    sample_time = time.time() - start_time
    print("steps: %d,  total time: %.2f,  step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    total_loss, value = [0 for _ in range(n)], [0 for _ in range(n)]
    if train:
        print("===== train =====")
        start_time = time.time()
        for i in range(n):
            replay_queues[i].append(sample_buffer[i])
            total_loss[i], value[i] = models[i].train(replay_queues[i])
        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)

    def round_list(l): return [round(x, 2) for x in l]
    return round_list(total_loss), nums, round_list(total_reward), round_list(value)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=5)
    parser.add_argument("--render_every", type=int, default=10)
    parser.add_argument("--n_round", type=int, default=800)
    parser.add_argument("--render", action="store_true")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--map_size", type=int, default=100)
    parser.add_argument("--name", type=str, default="battle")
    parser.add_argument("--greedy", action="store_true")
    args = parser.parse_args()

    # set logger
    log.basicConfig(level=log.INFO, filename=args.name + '.log')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init the game
    env = gw.GridWorld(load_config(args.map_size))
    env.set_render_dir("build/render")

    # two groups of animal
    handles = env.get_handles()

    # init two models
    models = [
        RLModel(env, handles[0], args.name + '-l', batch_size=512, target_freq=500),
        RLModel(env, handles[1], args.name + "-r", batch_size=512, target_freq=500)
    ]
    replay_queues = [EpisodesBuffersQueue(len=12) for _ in range(len(handles))]

    # load if
    savedir = 'save_model'
    if args.load_from is not None:
        start_from = args.load_from
        print("load ... %d" % start_from)
        for model in models:
            model.load(savedir, start_from)
    else:
        start_from = 0

    # print debug info
    print(args)
    print("view_size", env.get_view_space(handles[0]))

    # play
    for k in range(start_from, start_from + args.n_round):
        start_time = time.time()
        eps = max(0.10, 1 - k * 0.02) if not args.greedy else 0
        loss, num, reward, value = play_a_round(env, args.map_size, handles, models, replay_queues,
                                                print_every=50, train=args.train,
                                                render=args.render or (k+1) % args.render_every == 0,
                                                eps=eps)  # for e-greedy
        log.info("round %d\t loss: %s\t num: %s\t reward: %s\t value: %s" % (k, loss, num, reward, value))
        print("round time %.2f\n" % (time.time() - start_time))

        if (k + 1) % args.save_every == 0 and args.train:
            print("save model... ")
            if not os.path.exists(savedir):
                os.mkdir(savedir)
            for model in models:
                model.save(savedir, k)

