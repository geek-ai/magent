import argparse
import time
import os
import logging as log
from multiprocessing import Process, Pipe

import numpy as np

import magent.gridworld as gw
from magent.builtin.tf_model import DeepQNetwork as RLModel
from magent.utility import model_client, piecewise_decay


def load_config(map_size):
    cfg = gw.Config()

    cfg.set({"map_width": map_size, "map_height": map_size})

    predator = cfg.add_agent_type(
        "predator",
        {'width': 2, 'length': 2, 'hp': 1, 'speed': 1,
         'view_range': gw.CircleRange(5), 'attack_range': gw.CircleRange(2),
         'step_reward': 0, 'attack_penalty': -0.2})

    prey = cfg.add_agent_type(
        "prey",
        {'width': 1, 'length': 1, 'hp': 1, 'speed': 1.5,
         'view_range': gw.CircleRange(4), 'attack_range': gw.CircleRange(0),
         'step_reward': 0})

    predator_group  = cfg.add_group(predator)
    prey_group = cfg.add_group(prey)

    a = gw.AgentSymbol(predator_group, index='any')
    b = gw.AgentSymbol(prey_group, index='any')

    cfg.add_reward_rule(gw.Event(a, 'attack', b), receiver=[a, b], value=[1, -1])

    return cfg


def play_a_round(env, map_size, handles, conns, print_every, train=True, render=False, eps=None):

    env.reset()

    env.add_walls(method="random", n=map_size * map_size * 0.03)
    env.add_agents(handles[0], method="random", n=map_size * map_size * 0.0125)
    env.add_agents(handles[1], method="random", n=map_size * map_size * 0.025)

    step_ct = 0
    done = False

    n = len(handles)
    obs  = [[] for _ in range(n)]
    ids  = [[] for _ in range(n)]
    acts = [[] for _ in range(n)]
    nums = [env.get_num(handle) for handle in handles]
    total_reward = [0 for _ in range(n)]

    print("===== sample =====")
    print("eps %s number %s" % (eps, nums))
    start_time = time.time()
    while not done:
        # take actions for every model
        for i in range(n):
            obs[i] = env.get_observation(handles[i])
            ids[i] = env.get_agent_id(handles[i])
            conns[i].send(['act', obs[i], ids[i], 'e_greedy', eps])
        for i in range(n):
            acts[i] = conns[i].recv()
            env.set_action(handles[i], acts[i])

        # simulate one step
        done = env.step()

        # sample
        step_reward = []
        for i in range(n):
            rewards = env.get_reward(handles[i])
            if train:
                alives  = env.get_alive(handles[i])
                conns[i].send(["sample", rewards, alives])
            s = sum(rewards)
            step_reward.append(s)
            total_reward[i] += s

        # render
        if render:
            env.render()

        # clear dead agents
        env.clear_dead()

        if step_ct % print_every == 0:
            print("step %3d,  reward: %s,  total_reward: %s " %
                  (step_ct, np.around(step_reward, 2), np.around(total_reward, 2)))
        step_ct += 1

        # check 'done' returned by 'sample' command
        if train:
            for conn in conns:
                assert conn.recv() == "done"

        if step_ct > 250:
            break

    sample_time = time.time() - start_time
    print("steps: %d,  total time: %.2f,  step average %.2f" % (step_ct, sample_time, sample_time / step_ct))

    # train
    total_loss, value = [0 for _ in range(n)], [0 for _ in range(n)]
    if train:
        print("===== train =====")
        start_time = time.time()

        for i in range(n):
            conns[i].send(['train', 2000])
        for i in range(n):
            total_loss[i], value[i] = conns[i].recv()

        train_time = time.time() - start_time
        print("train_time %.2f" % train_time)

    def round_list(l): return [round(x, 2) for x in l]
    return round_list(total_loss), round_list(total_reward), round_list(value)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--save_every", type=int, default=2)
    parser.add_argument("--render_every", type=int, default=10)
    parser.add_argument("--n_round", type=int, default=500)
    parser.add_argument("--render", action="store_true")
    parser.add_argument("--load_from", type=int)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--map_size", type=int, default=1000)
    parser.add_argument("--greedy", action="store_true")
    parser.add_argument("--name", type=str, default="pursuit")
    args = parser.parse_args()

    # set logger
    log.basicConfig(level=log.INFO, filename=args.name + '.log')
    console = log.StreamHandler()
    console.setLevel(log.INFO)
    log.getLogger('').addHandler(console)

    # init the game
    env = gw.GridWorld(load_config(args.map_size))
    env.set_render_dir("build/render")

    # two groups of agents
    names = ["predator", "prey"]
    handles = env.get_handles()

    # start group controller processings
    pipes = [Pipe() for _ in range(len(handles))]
    pros = [
        Process(target=model_client,
                args=(pipes[0][1], env, handles[0], RLModel,
                      {'name': names[0], 'train_freq': 1, 'batch_size': 512},
                      3, 4000)),  # replay queue length, replay buffer size
        Process(target=model_client,
                args=(pipes[1][1], env, handles[1], RLModel,
                      {'name': names[1], 'train_freq': 1, 'batch_size': 512},
                      3, 4000)),  # replay queue length, replay buffer size
    ]

    conns = [pipe[0] for pipe in pipes]
    for processing in pros:
        processing.start()

    # load if
    savedir = 'save_model'
    if args.load_from is not None:
        start_from = args.load_from
        print("load ... %d" % start_from)
        for conn in conns:    # send 'load' command
            conn.send(["load", savedir, start_from])
        for conn in conns:    # receive 'done'
            assert conn.recv() == "done"
    else:
        start_from = 0

    # print debug info
    print(args)
    print("view_space", env.get_view_space(handles[0]))
    print("feature_space", env.get_feature_space(handles[0]))

    # play
    start = time.time()
    for k in range(start_from, start_from + args.n_round):
        tic = time.time()
        eps = piecewise_decay(k, [0, 200, 400], [1, 0.2, 0.05]) if not args.greedy else 0

        loss, reward, value = play_a_round(env, args.map_size, handles, conns,
                                           print_every=50, train=args.train,
                                           render=args.render or (k+1) % args.render_every == 0,
                                           eps=eps)  # for e-greedy
        log.info("round %d\t loss: %s\t reward: %s\t value: %s" % (k, loss, reward, value))
        print("round time %.2f  total time %.2f\n" % (time.time() - tic, time.time() - start))

        if (k + 1) % args.save_every == 0 and args.train:
            print("save model... ")
            if not os.path.exists(savedir):
                os.mkdir(savedir)
            for conn in conns:   # send 'save' command
                conn.send(["save", savedir, k])
            for conn in conns:   # receive 'done'
                assert conn.recv() == "done"

    # send quit command
    for conn in conns:
        conn.send(["quit"])

