# MAgent
MAgent is a platform aimed at many-agent reinforcement learning.  
[see video](https://www.youtube.com/watch?v=HCSm0kVolqI)

## Get Started
[run your first demo](https://bitbucket.org/geek-ai/magent/wiki/Get%20started)

## Examples

* **pursuit**

	```
	python train_pursuit --train
	```

* **gathering**

	```
	python train_gather --train
	```

* **battle**

	```
	python train_battle --train
	```

Video files will be saved every 10 rounds. You can use render to see them.

## API
see api_demo.py for the usage of API.
