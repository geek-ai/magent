//
// Created by mercy on 17-7-9.
//

#include "Environment.h"
#include "runtime_api.h"
#include "gridworld/GridWorld.h"
#include "discrete_snake/DiscreteSnake.h"

/**
 *  General Environment
 */
// game
int env_new_game(EnvHandle *game, const char *name) {
    using ::magent::utility::strequ;

    if (strequ(name, "GridWorld")) {
        *game = new ::magent::gridworld::GridWorld();
    } else if (strequ(name, "DiscreteSnake")) {
        *game = new ::magent::discrete_snake::DiscreteSnake();
    } else {
        throw std::invalid_argument("invalid name of game");
    }
    return 0;
}

int env_delete_game(EnvHandle game) {
    TRACE_PRINT("env delete game.  ");
    delete game;
    TRACE_PRINT("delete done");
    return 0;
}

int env_config_game(EnvHandle game, const char *name, void *p_value) {
    TRACE_PRINT("env config game.  ");
    game->set_config(name, p_value);
    return 0;
}

// run step
int env_reset(EnvHandle game) {
    TRACE_PRINT("env reset.  ");
    game->reset();
    return 0;
}

int env_get_observation(EnvHandle game, GroupHandle group, float **buffer) {
    TRACE_PRINT("env get observation.  ");
    game->get_observation(group, buffer);
    return 0;
}

int env_set_action(EnvHandle game, GroupHandle group, const int *actions) {
    TRACE_PRINT("env set action.  ");
    game->set_action(group, actions);
    return 0;
}

int env_step(EnvHandle game, int *done) {
    TRACE_PRINT("env step.  ");
    game->step(done);
    return 0;
}

int env_get_reward(EnvHandle game, GroupHandle group, float *buffer) {
    TRACE_PRINT("env get reward.  ");
    game->get_reward(group, buffer);
    return 0;
}

// info getter
int env_get_info(EnvHandle game, GroupHandle group, const char *name, void *buffer) {
    TRACE_PRINT("env get info %s. ", name);
    game->get_info(group, name, buffer);
    return 0;
}

// render
int env_render(EnvHandle game) {
    TRACE_PRINT("env render.\n ");
    game->render();
    return 0;
}

int env_render_next_file(EnvHandle game) {
    TRACE_PRINT("env render next file.\n ");
    // temporally only needed in DiscreteSnake
    ((::magent::discrete_snake::DiscreteSnake *)game)->render_next_file();
    return 0;
}

/**
 *  GridWorld special
 */
// agent
int gridworld_register_agent_type(EnvHandle game, const char *name, int n,
                                  const char **keys, float *values) {
    TRACE_PRINT("gridworld register agent type.  ");
    ((::magent::gridworld::GridWorld *)game)->register_agent_type(name, n, keys, values);
    return 0;
}

int gridworld_new_group(EnvHandle game, const char *agent_type_Name, GroupHandle *group) {
    TRACE_PRINT("gridworld new group.  ");
    ((::magent::gridworld::GridWorld *)game)->new_group(agent_type_Name, group);
    return 0;
}

int gridworld_add_agents(EnvHandle game, GroupHandle group, int n, const char *method,
                         const int *pos_x, const int *pos_y, const int *dir) {
    TRACE_PRINT("gridworld add agents.  ");
    ((::magent::gridworld::GridWorld *)game)->add_agents(group, n, method, pos_x, pos_y, dir);
    return 0;
}

// run step
int gridworld_clear_dead(EnvHandle game) {
    TRACE_PRINT("gridworld clear dead\n");
    ((::magent::gridworld::GridWorld *)game)->clear_dead();
    return 0;
}

int gridworld_set_goal(EnvHandle game, GroupHandle group, const char *method, const int *linear_buffer) {
    TRACE_PRINT("gridworld clear dead\n");
    ((::magent::gridworld::GridWorld *)game)->set_goal(group, method, linear_buffer);
    return 0;
}

// reward description
int gridworld_define_agent_symbol(EnvHandle game, int no, int group, int index) {
    printf("define agent symbol %d (group=%d index=%d)\n", no, group, index);
    ((::magent::gridworld::GridWorld *)game)->define_agent_symbol(no, group, index);
    return 0;
}

int gridworld_define_event_node(EnvHandle game, int no, int op, int *inputs, int n_inputs) {
    printf("define event node %d op=%d inputs=[", no, op);
    for (int i = 0; i < n_inputs; i++)
        printf("%d, ", inputs[i]);
    printf("]\n");
    ((::magent::gridworld::GridWorld *)game)->define_event_node(no, op, inputs, n_inputs);
    return 0;
}

int gridworld_add_reward_rule(EnvHandle game, int on, int *receiver, float *value, int n_receiver, bool is_terminal) {
    printf("define rule on=%d rec,val=[", on);
    for (int i = 0; i < n_receiver; i++)
        printf("%d %g, ", receiver[i], value[i]);
    printf("]\n");
    ((::magent::gridworld::GridWorld *)game)->add_reward_rule(on, receiver, value, n_receiver, is_terminal);
    return 0;
}

/**
 * DiscreteSnake special
 */
int discrete_snake_add_object(EnvHandle game, int obj_id, int n, const char *method, const int *linear_buffer) {
    TRACE_PRINT("discrete snake add object\n");
    ((::magent::discrete_snake::DiscreteSnake *)game)->add_object(obj_id, n, method, linear_buffer);
    return 0;
}

// run step
int discrete_snake_clear_dead(EnvHandle game) {
    TRACE_PRINT("gridworld clear dead\n");
    ((::magent::discrete_snake::DiscreteSnake *)game)->clear_dead();
    return 0;
}




/************************ SPLIT ************************/
/************************ SPLIT ************************/
/************************ SPLIT ************************/
/*
 * Temp C Booster
 * C lib for temporary python code, to make them faster
 * mainly for rule-based agents
 */
#include <algorithm>

void runaway_infer_action(float *obs_buf, float *feature_buf, int n, int height, int width, int n_channel,
                 int attack_base, int *act_buf, int away_channel, int move_back) {
    float (*observation)[height][width][n_channel] = (float (*)[height][width][n_channel])obs_buf;

    int wall  = 0;
    int food  = 1;

    #pragma omp parallel for
    for (int i = 0; i < n; i++) {
        float (*obs)[width][n_channel] = (float (*)[width][n_channel])observation[i];

        bool found = false;
        for (int row = height-3; row <= height-1 && !found; row++) {
            for (int col = width/2 - 1; col <= width/2 + 1 && !found; col++) {
                if (obs[row][col][away_channel] > 0.5) {
                    found = true;
                }
            }
        }
        if (found) {
            act_buf[i] = move_back;
        } else {
            act_buf[i] = move_back + 1;
        }
    }
}

void rush_prey_infer_action(float *obs_buf, float *feature_buf, int n, int height, int width, int n_channel,
                            int *act_buf, int attack_channel, int attack_base,
                            int *view2attack_buf, float threshold) {
    float (*observation)[height][width][n_channel] = (float (*)[height][width][n_channel])obs_buf;

    int enemy = attack_channel;
    int wall  = 0;
    int food  = 1;

    #pragma omp parallel for
    for (int i = 0; i < n; i++) {
        if (feature_buf[i] < threshold) {
            float (*obs)[width][n_channel] = (float (*)[width][n_channel])observation[i];

            int (*view2attack)[width] = (int (*)[width]) view2attack_buf;

            int action = -1;
            bool found = false;
            bool found_attack = false;

            for (int row = 0; row < height; row++) {
                for (int col = 0; col < width; col++) {
                    if (obs[row][col][enemy] > 0.5 || obs[row][col][food] > 0.5) {
                        found = true;
                        if (view2attack[row][col] != -1) {
                            found_attack = true;
                            action = view2attack[row][col];
                            break;
                        }
                    }
                }
                if (found_attack)
                    break;
            }

            if (action == -1) {
                if (found && int(obs[height-1][width/2][wall] + 0.5) != 1)
                    act_buf[i] = 0;
                else
                    act_buf[i] = (int)random() % (attack_base);
            } else
                act_buf[i] = attack_base + action;
        } else {
            act_buf[i] = (int)random() % (attack_base);
        }
    }
}

int get_action(const std::pair<int, int> &disp, bool stride) {
	int action = -1;
    if (disp.first < 0) {
        if (disp.second < 0) {
            action = 1;
        } else if (disp.second == 0) {
            action = stride ? 0 : 2;
        } else {
            action = 3;
        }
    } else if (disp.first == 0) {
        if (disp.second < 0) {
            action = stride ? 4 : 5;
        } else if (disp.second == 0) {
        	action = 6;
        } else {
            action = stride ? 8 : 7;
        }
    } else {
        if (disp.second < 0) {
            action = 9;
        } else if (disp.second == 0) {
            action = stride ? 12 : 10;
        } else {
            action = 11;
        }
    }
    return action;
}

void gather_infer_action(float *obs_buf, float *hp_buf, int n, int height, int width, int n_channel,
                         int *act_buf, int attack_base, int *view2attack_buf) {
	float (*observations)[height][width][n_channel] = (float (*)[height][width][n_channel])obs_buf;
    int (*view2attack)[width] = (int (*)[width]) view2attack_buf;
    
    #pragma omp parallel for
    for (int i = 0; i < n; i++) {
        float (*observation)[width][n_channel];
        observation = observations[i];
    	int action = -1;

    	if (action == -1) {
    		std::vector<int> att_vector;
    		std::vector<std::pair<int, int>> vector;

            // find food
    		for (int row = 0; row < height; row++)
    			for (int col = 0; col < width; col++) {
                    if (fabs(observation[row][col][4] - 1.0) < 1e-10) {
                    	if (view2attack[row][col] != -1) {
                        	att_vector.push_back(view2attack[row][col] + attack_base);
                        } else {
                            int d_row = row - height/2, d_col = col - width/2;
                            if (d_row == d_col && abs(d_col) == 1) {
                                if (rand() & 1)
                                    d_row = 0;
                                else
                                    d_col = 0;
                            }
                        	vector.push_back(std::make_pair(d_row, d_col));
                        }
                    }
    			}
    		if (!att_vector.empty()) {
    			action = att_vector[rand() % att_vector.size()];
    		} else if (!vector.empty()) {
    			action = get_action(vector[0], false);
    		}
    	}

        // use minimap to navigation
    	if (action == -1) {
			std::pair<int, int> mypos = std::make_pair(-1, -1);
			std::vector<std::pair<float, std::pair<int, int>>> vector;

			for (int row = 0; row < height; row++)
				for (int col = 0; col < width; col++) {
					if (observation[row][col][3] > 1.0) {
						mypos = std::make_pair(row, col);
					}
				}

			for (int row = 0; row < height; row++)
				for (int col = 0; col < width; col++) {
					if (observation[row][col][6] > 0.0) {
						vector.push_back(std::make_pair(observation[row][col][6], std::make_pair(row - mypos.first, col - mypos.second)));
					}
				}
			std::sort(vector.rbegin(), vector.rend());
    		action = get_action(vector[rand() % vector.size()].second, true);
            if (action == 6) {
                action = rand() % attack_base;
            }
    	}
    	
    	act_buf[i] = action;
    }
}
