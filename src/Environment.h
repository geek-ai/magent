//
// Created by mercy on 17-7-9.
//

#ifndef MAGNET_ENVIRONMENT_H
#define MAGNET_ENVIRONMENT_H

namespace magent {
namespace environment {

typedef int GroupHandle;

class Environment {
public:
    Environment() = default;
    virtual ~Environment() = default;

    // game
    virtual void set_config(const char *key, void *p_value) = 0;

    // run step
    virtual void reset() = 0;
    virtual void get_observation(GroupHandle group, float **buffer) = 0;
    virtual void set_action(GroupHandle group, const int *actions) = 0;
    virtual void step(int *done) = 0;
    virtual void get_reward(GroupHandle group, float *buffer) = 0;

    // info getter
    virtual void get_info(GroupHandle group, const char *name, void *buffer) = 0;

    // render
    virtual void render() = 0;
};

typedef Environment* EnvHandle;

} // namespace environment
} // namespace magent


/*
 * simple logger
 */
#define PRINT_TRACE_LOG 0
#define PRINT_DEBUG_LOG 0
#define PRINT_INFO_LOG  0
#define PRINT_WARN_LOG 1

#if PRINT_TRACE_LOG
#define TRACE_PRINT(...) fprintf(stderr, __VA_ARGS__);
#else
#define TRACE_PRINT(...) ;
#endif

#if PRINT_DEBUG_LOG
#define DEBUG_PRINT(...) fprintf(stderr, __VA_ARGS__);
#else
#define DEBUG_PRINT(...) ;
#endif

#if PRINT_INFO_LOG
#define INFO_PRINT(...) fprintf(stdout, __VA_ARGS__);
#else
#define INFO_PRINT(...) ;
#endif

#if PRINT_WARN_LOG
#define WARN_PRINT(...) fprintf(stdout, __VA_ARGS__);
#else
#define WARN_PRINT(...) ;
#endif



#endif //MAGNET_ENVIRONMENT_H
