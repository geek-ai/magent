#include <iostream>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <omp.h>
#include <fstream>
#include <assert.h>

#include "GridWorld.h"
#include "reward_description.h"

namespace magent {
namespace gridworld {

GridWorld::GridWorld(){
    //omp_set_nested(1);
    first_render = true;

    food_mode = false;
    turn_mode = false;
    minimap_mode = false;
    goal_mode = false;
    revive_mode = false;
    large_map_mode = false;

    reward_des_initialized = false;
    embedding_size = 0;
    random_engine.seed(0);
}

GridWorld::~GridWorld(){
    for (int i = 0; i < groups.size(); i++) {
        std::vector<Agent*> &agents = groups[i].get_agents();

        // free agents
        size_t agent_size = agents.size();
        #pragma omp parallel for
        for (int j = 0; j < agent_size; j++) {
            delete agents[j];
        }

        // free ranges
        AgentType &type = groups[i].get_type();
        if (type.view_range != nullptr) {
            delete type.view_range;
            type.view_range = nullptr;
        }
        if (type.attack_range != nullptr) {
            delete type.attack_range;
            type.attack_range = nullptr;
        }
        if (type.move_range != nullptr) {
            delete type.move_range;
            type.move_range = nullptr;
        }
    }
}

void GridWorld::reset() {
    id_counter = 0;
    map.reset(width, height, food_mode);
    render_generator.next_file();
    stat_recorder.reset();

    for (int i = 0;i < groups.size(); i++) {
        std::vector<Agent*> &agents = groups[i].get_agents();

        // free agents
        size_t agent_size = agents.size();
        #pragma omp parallel for
        for (int j = 0; j < agent_size; j++) {
            delete agents[j];
        }

        groups[i].clear();
        groups[i].get_type().n_channel = group2channel((GroupHandle)groups.size());
    }

    if (!reward_des_initialized) {
        init_reward_description();
        reward_des_initialized = true;
    }
}

void GridWorld::set_config(const char *key, void *p_value) {
    float fvalue = *(float *)p_value;
    int ivalue   = *(int *)p_value;
    bool bvalue   = *(bool *)p_value;
    const char *strvalue = (const char *)p_value;

    if (strequ(key, "map_width"))
        width = ivalue;
    else if (strequ(key, "map_height"))
        height = ivalue;

    else if (strequ(key, "food_mode"))  // dead agent will leave food in the map
        food_mode = bvalue;
    else if (strequ(key, "turn_mode"))  // has two more actions -- turn left and turn right
        turn_mode = bvalue;
    else if (strequ(key, "minimap_mode")) // add minimap into observation
        minimap_mode = bvalue;
    else if  (strequ(key, "revive_mode"))
        revive_mode = bvalue;
    else if (strequ(key, "goal_mode"))  // every agents has a specific goal
        goal_mode = bvalue;
    else if (strequ(key, "embedding_size")) // embedding size in the observation.feature
        embedding_size = ivalue;

    else if (strequ(key, "render_dir")) // the directory of saved videos
        render_generator.set_render("save_dir", strvalue);
    else if (strequ(key, "seed"))
        random_engine.seed((unsigned long)ivalue);
    else
        throw std::invalid_argument("invalid argument in GridWorld::set_render");
}

void GridWorld::register_agent_type(const char *name, int n, const char **keys, float *values) {
    std::string str(name);

    if (agent_types.find(str) != agent_types.end())
        throw std::invalid_argument("duplicated name of agent type in GridWorld::register_agent_type");

    agent_types.insert(std::make_pair(str, AgentType(n, str, keys, values, turn_mode)));
}

void GridWorld::new_group(const char* agent_name, GroupHandle *group) {
    *group = (GroupHandle)groups.size();

    auto iter = agent_types.find(std::string(agent_name));
    if (iter == agent_types.end()) {
        throw std::invalid_argument("invalid name of agent in new_group");
    } else {
        groups.push_back(Group(iter->second));
    }
}

void add_or_error(int ret, int x, int y, int &id_counter, Group &g, Agent *agent) {
    if (ret != 0) {
        ;//fprintf(stderr, "invalid position in add_agents (%d,%d), already occupied, ignored.\n", x, y);
    } else {
        id_counter++;
        g.add_agent(agent);
    }
};

void GridWorld::add_agents(GroupHandle group, int n, const char *method,
                           const int *pos_x, const int *pos_y, const int *pos_dir) {
    int ret;

    if (group == -1) {  // group == -1 for wall
        if (strequ(method, "random")) {
            for (int i = 0; i < n; i++) {
                Position pos = map.get_random_blank(random_engine);
                ret = map.add_wall(pos);
                if (ret != 0)
                    WARN_PRINT("invalid position in add_wall (%d,%d), already occupied, ignored.\n",
                            pos_x[i], pos_y[i]);
            }
        } else if (strequ(method, "custom")) {
            for (int i = 0; i < n; i++) {
                Position pos = Position{pos_x[i], pos_y[i]};
                ret = map.add_wall(pos);
                if (ret != 0)
                    WARN_PRINT("invalid position in add_wall (%d,%d), already occupied, ignored.\n",
                           pos_x[i], pos_y[i]);
            }
        } else if (strequ(method, "fill")) {
            // parameter int xs[4] = {x, y, width, height}
            int x_start = pos_x[0],         y_start = pos_x[1];
            int x_end = x_start + pos_x[2], y_end = y_start + pos_x[3];
            for (int x = x_start; x < x_end; x++)
                for (int y = y_start; y < y_end; y++) {
                    ret = map.add_wall(Position{x, y});
                    if (ret != 0)
                        WARN_PRINT("invalid position in add_wall (%d,%d), already occupied, ignored.\n", x, y);
                }
        } else {
            throw std::invalid_argument("unsupported method in GridWorld::add_agents");
        }
    } else {  // group >= 0 for agents
        if (group > groups.size()) {
            throw std::invalid_argument("invalid group handle in GridWorld::add_agents");
        }
        Group &g = groups[group];
        int base_channel_id = group2channel(group);
        int width = g.get_type().width, length = g.get_type().length;
        AgentType &agent_type = g.get_type();

        if (strequ(method, "random")) {
            for (int i = 0; i < n; i++) {
                Agent *agent = new Agent(agent_type, id_counter, group);
                Direction dir = turn_mode ? (Direction)(random_engine() % DIR_NUM) : NORTH;
                Position pos;

                if (dir == NORTH || dir == SOUTH) {
                    pos = map.get_random_blank(random_engine, width, length);
                } else {
                    pos = map.get_random_blank(random_engine, length, width);
                }

                agent->set_dir(dir);
                agent->set_pos(pos);

                ret = map.add_agent(agent, base_channel_id);
                add_or_error(ret, pos.x, pos.y, id_counter, g, agent);
            }
        } else if (strequ(method, "custom")) {
            for (int i = 0; i < n; i++) {
                Agent *agent = new Agent(agent_type, id_counter, group);

                if (pos_dir[i] >= DIR_NUM) {
                    throw std::invalid_argument("invalid direction in add_agent");
                }

                agent->set_dir(turn_mode ? (Direction) pos_dir[i] : NORTH);
                agent->set_pos((Position) {pos_x[i], pos_y[i]});

                ret = map.add_agent(agent, base_channel_id);
                add_or_error(ret, pos_x[i], pos_y[i], id_counter, g, agent);
            }
        } else if (strequ(method, "fill")) {
            // parameter int xs[4] = {x, y, width, height}
            int x_start = pos_x[0],         y_start = pos_x[1];
            int x_end = x_start + pos_x[2], y_end = y_start + pos_x[3];
            Direction dir = turn_mode ? (Direction)pos_x[4] : NORTH;
            int m_width, m_height;

            if (dir == NORTH || dir == SOUTH) {
                m_width = width;
                m_height = length;
            } else if (dir == WEST || dir == EAST) {
                m_width = length;
                m_height = width;
            } else {
                throw std::invalid_argument("invalid direction in add_agent");
            }

            for (int x = x_start; x < x_end; x += m_width)
                for (int y = y_start; y < y_end; y += m_height) {
                    Agent *agent = new Agent(agent_type, id_counter, group);

                    agent->set_pos(Position{x, y});
                    agent->set_dir(dir);

                    ret = map.add_agent(agent, base_channel_id);
                    add_or_error(ret, x, y, id_counter, g, agent);
                }
        } else {
            throw std::invalid_argument("unsupported method in GridWorld::add_agents");
        }
    }
}

void GridWorld::get_observation(GroupHandle group, float **linear_buffer) {
    Group &g = groups[group];
    AgentType &type = g.get_type();

    const int n_channel   = g.get_type().n_channel;
    const int view_width  = g.get_type().view_range->get_width();
    const int view_height = g.get_type().view_range->get_height();
    const int n_group = (int)groups.size();
    const int n_action = (int)type.action_space.size();
    int feature_space = embedding_size + n_action ; // embedding + last action
    if (goal_mode) {
        feature_space += 2;
    }
    if (minimap_mode) // + coordination
        feature_space += 2;

    std::vector<Agent*> &agents = g.get_agents();
    float (*view_buffer)[view_height][view_width][n_channel];
    float (*feature_buffer)[feature_space];
    view_buffer = (float (*)[view_height][view_width][n_channel])linear_buffer[0];
    feature_buffer = (float (*)[feature_space])linear_buffer[1];

    size_t agent_size = agents.size();

    memset(view_buffer, 0, sizeof(float) * agent_size * view_height * view_width * n_channel);
    memset(feature_buffer, 0, sizeof(float) * agent_size * feature_space);

    // gather view info from AgentType
    const Range *range = type.view_range;
    int view_x_offset = type.view_x_offset, view_y_offset = type.view_y_offset;
    int view_left_top_x, view_left_top_y, view_right_bottom_x, view_right_bottom_y;
    range->get_range_rela_offset(view_left_top_x, view_left_top_y,
                                 view_right_bottom_x, view_right_bottom_y);

    // to make channel layout in observation symmetric to every group
    std::vector<int> channel_trans((unsigned int)type.n_channel);
    int base = group2channel(0);
    for (int i = 0; i < base; i++)
        channel_trans[i] = i;
    for (int i = 0; i < groups.size(); i++) {
        int cycle_group = (group + i) % n_group;
        channel_trans[group2channel(cycle_group)] = base;
        if (minimap_mode) {
            base += 3;
        } else {
            base += 2;
        }
    }

    // build minimap
    float *minimap_buffer = nullptr;
    float (*minimap)[view_width][n_group];
    int scale_h = (height + view_height - 1) / view_height;
    int scale_w = (width + view_width - 1) / view_width;

    if (minimap_mode) {
        minimap_buffer = new float [view_height * view_width * n_group];
        memset(minimap_buffer, 0, sizeof(float) * view_height * view_width * n_group);
        minimap = (float (*)[view_width][n_group])minimap_buffer;

        std::vector<int> group_sizes;
        for (int i = 0; i < n_group; i++)
            group_sizes.push_back(groups[i].get_size() > 0 ? groups[i].get_size() : 1);

        // by agents
        #pragma omp parallel for
        for (int i = 0; i < n_group; i++) {
            std::vector<Agent*> &agents_ = groups[i].get_agents();
            for (int j = 0; j < agents_.size(); j++) {
                Position pos = agents_[j]->get_pos();
                int x = pos.x / scale_w, y = pos.y / scale_h;
                minimap[y][x][i]++;
            }
            // scale
            for (int j = 0; j < view_height; j++) {
                for (int k = 0; k < view_width; k++) {
                    minimap[j][k][i] /= group_sizes[i];
                }
            }
        }
    }

    // fill local view
    #pragma omp parallel for
    for (int i = 0; i < agent_size; i++) {
        Agent *agent = agents[i];
        // get view
        map.extract_view(agent, (float *)view_buffer[i], &channel_trans[0], range,
            n_channel, view_width, view_height, view_x_offset, view_y_offset,
            view_left_top_x, view_left_top_y, view_right_bottom_x, view_right_bottom_y);

        if (minimap_mode) {
            int self_x = agent->get_pos().x / scale_w;
            int self_y = agent->get_pos().y / scale_h;
            for (int j = 0; j < n_group; j++) {
                int minimap_channel = channel_trans[group2channel(j)] + 2;

                // copy minimap to channel
                for (int k = 0; k < view_height; k++) {
                    for (int l = 0; l < view_width; l++) {
                        view_buffer[i][k][l][minimap_channel] = minimap[k][l][j];
                    }
                }
                view_buffer[i][self_y][self_x][minimap_channel] += 1;
            }
        }

        // get
        agent->get_embedding(feature_buffer[i], embedding_size);
        Position pos = agent->get_pos();
        if (agent->get_action() != n_action)
            feature_buffer[i][embedding_size + agent->get_action()] = 1;
        if (minimap_mode) {
            feature_buffer[i][embedding_size + n_action]     = (float) pos.x / width;
            feature_buffer[i][embedding_size + n_action + 1] = (float) pos.y / height;
        }

        /*if (goal_mode) {
            Position goal;
            int radius;
            agent->get_goal(goal, radius);
            //printf("%d %d\n", goal.x, goal.y);
            feature_buffer[i][embedding_size + n_action + 2] = ((float) (pos.x - goal.x)) / width;
            feature_buffer[i][embedding_size + n_action + 3] = ((float) (pos.y - goal.y)) / height;
        }*/
    }

    if (minimap_buffer != nullptr)
        delete [] minimap_buffer;
}

void GridWorld::set_action(GroupHandle group, const int *actions) {
    std::vector<Agent*> &agents = groups[group].get_agents();
    const AgentType &type = groups[group].get_type();
    // action space layout : move turn attack ...
    const int bandwidth = (width + NUM_MOVE_BUFFER - 1) / NUM_MOVE_BUFFER;

    size_t agent_size = agents.size();

    if (large_map_mode) { // divide and compute in parallel
        for (int i = 0; i < agent_size; i++) {
            Agent *agent = agents[i];
            Action act = (Action) actions[i];
            agent->set_action(act);

            if (act < type.turn_base) {          // move
                int x = agent->get_pos().x;
                int x_ = x % bandwidth;
                if (x_ < 3 || x_ > bandwidth - 3) {
                    move_buffer_bound.push_back(MoveAction{agent, act - type.move_base});
                } else {
                    int to = agent->get_pos().x / bandwidth;
                    move_buffers[to].push_back(MoveAction{agent, act - type.move_base});
                }
            } else if (act < type.attack_base) { // turn
                int x = agent->get_pos().x;
                int x_ = x % bandwidth;
                if (x_ < 3 || x_ > bandwidth - 3) {
                    turn_buffer_bound.push_back(TurnAction{agent, act - type.move_base});
                } else {
                    int to = agent->get_pos().x / bandwidth;
                    turn_buffers[to].push_back(TurnAction{agent, act - type.move_base});
                }
            } else {                             // attack
                attack_buffer.push_back(AttackAction{agent, act - type.attack_base});
            }
        }
    } else {
        for (int i = 0; i < agent_size; i++) {
            Agent *agent = agents[i];
            Action act = (Action) actions[i];
            agent->set_action(act);

            if (act < type.turn_base) {          // move
                move_buffer_bound.push_back(MoveAction{agent, act - type.move_base});
            } else if (act < type.attack_base) { // turn
                turn_buffer_bound.push_back(TurnAction{agent, act - type.move_base});
            } else {                             // attack
                attack_buffer.push_back(AttackAction{agent, act - type.attack_base});
            }
        }
    }
}

void GridWorld::step(int *done) {
    #pragma omp declare reduction (merge : std::vector<RenderAttackEvent> : omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end()))
    const bool stat = false;

    TRACE_PRINT("gridworld step begin.  ");

    size_t attack_size = attack_buffer.size();
    size_t group_size  = groups.size();

    // shuffle attacks
    for (int i = 0; i < attack_size; i++) {
        int j = (int)random_engine() % (i+1);
        std::swap(attack_buffer[i], attack_buffer[j]);
    }

    TRACE_PRINT("attack.  ");
    std::vector<RenderAttackEvent> render_attack_buffer;

    // for statistic info
    std::map<PositionInteger, int> attack_obj_counter;

    // attack
    #pragma omp paralell for reduction(merge: attack_events)
    for (int i = 0; i < attack_size; i++) {
        Agent *agent = attack_buffer[i].agent;

        if (agent->is_dead())
            continue;

        int obj_x, obj_y;
        PositionInteger obj_pos = map.get_attack_obj(attack_buffer[i], obj_x, obj_y);
        if (!first_render)
            render_attack_buffer.emplace_back(RenderAttackEvent{agent->get_id(), obj_x, obj_y});

        if (obj_pos == -1) {  // attack blank block
            agent->add_reward(agent->get_type().attack_penalty);
            continue;
        }

        if (stat) {
            attack_obj_counter[obj_pos]++;
        }

        float reward = 0.0;
        GroupHandle dead_group = -1;
        #pragma omp critical
        {
            reward = map.do_attack(agent, dead_group, obj_pos);
            if (dead_group != -1) {
                groups[dead_group].inc_dead_ct();
            }
        }
        agent->add_reward(reward + agent->get_type().attack_penalty);
    }
    attack_buffer.clear();
    if (!first_render)
        render_generator.set_attack_event(render_attack_buffer);

    if (stat) {
        for (auto iter : attack_obj_counter) {
            if (iter.second > 1) {
                stat_recorder.both_attack++;
            }
        }
    }

    // starve
    TRACE_PRINT("starve.  ");
    for (int i = 0; i < group_size; i++) {
        Group &group = groups[i];
        std::vector<Agent*> &agents = group.get_agents();
        int starve_ct = 0;
        size_t agent_size = agents.size();

        #pragma omp parallel for reduction(+: starve_ct)
        for (int j = 0; j < agent_size; j++) {
            Agent *agent = agents[j];

            if (agent->is_dead())
                continue;

            // alive agents
            bool starve = agent->starve();
            if (starve) {
                map.remove_agent(agent);
                starve_ct++;
            }
        }
        group.set_dead_ct(group.get_dead_ct() + starve_ct);
    }

    if (turn_mode) {
        // do turn
        auto do_turn_for_a_buffer = [] (std::vector<TurnAction> &turn_buf, Map &map) {
            //std::random_shuffle(turn_buf.begin(), turn_buf.end());
            size_t turn_size = turn_buf.size();
            for (int i = 0; i < turn_size; i++) {
                Action act = turn_buf[i].action;
                Agent *agent = turn_buf[i].agent;

                if (agent->is_dead())
                    continue;

                INFO_PRINT("turn (%d,%d) %d\n", agent->get_pos().x, agent->get_pos().y, act);

                int dir = act * 2 - 1;
                map.do_turn(agent, dir);
            }
            turn_buf.clear();
        };

        if (large_map_mode) {
            TRACE_PRINT("turn parallel.  ");
            #pragma omp parallel for
            for (int i = 0; i < NUM_TURN_BUFFER; i++) {        // turn in separate areas, do them in parallel
                do_turn_for_a_buffer(turn_buffers[i], map);
            }
        }
        TRACE_PRINT("turn boundary.  ");
        do_turn_for_a_buffer(turn_buffer_bound, map);
    }

    // do move
    auto do_move_for_a_buffer = [] (std::vector<MoveAction> &move_buf, Map &map) {
        //std::random_shuffle(move_buf.begin(), move_buf.end());
        size_t move_size = move_buf.size();
        for (int j = 0; j < move_size; j++) {
            Action act = move_buf[j].action;
            Agent *agent = move_buf[j].agent;

            if (agent->is_dead())
                continue;

            INFO_PRINT("move (%d,%d) %d\n", agent->get_pos().x, agent->get_pos().y, act);

            int dx, dy;
            int delta[2];
            agent->get_type().move_range->num2delta(act, dx, dy);
            switch(agent->get_dir()) {
                case NORTH:
                    delta[0] = dx;  delta[1] = dy;  break;
                case SOUTH:
                    delta[0] = -dx; delta[1] = -dy; break;
                case WEST:
                    delta[0] = dy;  delta[1] = -dx; break;
                case EAST:
                    delta[0] = -dy; delta[1] = dx;  break;
                default:
                    throw std::runtime_error("invalid direction in GridWorld::step do move");
            }

            map.do_move(agent, delta);
        }
        move_buf.clear();
    };

    if (large_map_mode) {
        TRACE_PRINT("move parallel.  ");
        #pragma omp parallel for
        for (int i = 0; i < NUM_MOVE_BUFFER; i++) {    // move in separate areas, do them in parallel
            do_move_for_a_buffer(move_buffers[i], map);
        }
    }
    TRACE_PRINT("move boundary.  ");
    do_move_for_a_buffer(move_buffer_bound, map);

    TRACE_PRINT("calc_reward.  ");
    calc_reward();

    TRACE_PRINT("gameover check.  ");
    int live_ct = 0;  // default game over condition: all the agents in an arbitrary group die
    for (int i = 0; i < groups.size(); i++) {
        if (groups[i].get_alive_num() > 0)
            live_ct++;
    }
    *done = live_ct < groups.size();

    size_t rule_size = reward_rules.size();
    for (int i = 0; i < rule_size; i++) {
        if (reward_rules[i].trigger && reward_rules[i].is_terminal)
            *done = true;
    }
}

void GridWorld::clear_dead() {
    size_t group_size = groups.size();

    #pragma omp parallel for
    for (int i = 0; i < group_size; i++) {
        Group &group = groups[i];
        group.init_reward();
        std::vector<Agent*> &agents = group.get_agents();

        // clear dead agents
        size_t agent_size = agents.size();
        int dead_ct = 0;
        int pt = 0;
        float sum_x = 0, sum_y = 0;
        if (!revive_mode) {
            for (int j = 0; j < agent_size; j++) {
                Agent *agent = agents[j];
                if (agent->is_dead()) {
                    delete agent;
                    dead_ct++;
                } else {
                    agent->init_reward();
                    agent->set_index(pt);
                    agents[pt++] = agent;

                    //Position pos = agent->get_pos();
                    //sum_x += pos.x; sum_y += pos.y;
                }
            }
            agents.resize(pt);
        } else {
            AgentType &type = group.get_type();
            int width = type.width, length = type.length;
            int base_channel_id = group2channel(i);
            for (int j = 0; j < agent_size; j++) {
                Agent *agent = agents[j];
                if (agent->is_dead()) {
                    Direction dir = turn_mode ? (Direction)(random_engine() % (int)DIR_NUM) : NORTH;
                    Position pos;
                    if (dir == NORTH || dir == SOUTH) {
                        pos = map.get_random_blank(random_engine, width, length);
                    } else {
                        pos = map.get_random_blank(random_engine, length, width);
                    }

                    agent->set_dir(dir);
                    agent->set_pos(pos);
                    agent->revive();

                    map.add_agent(agent, base_channel_id);
                }
                agent->init_reward();
            }
        }
        group.set_dead_ct(0);
        //group.set_center(sum_x / pt, sum_y / pt);
    }
}

void GridWorld::set_goal(GroupHandle group, const char *method, const int *linear_buffer) {
    if (strequ(method, "random")) {
        std::vector<Agent*> &agents = groups[group].get_agents();
        for (int i = 0; i < agents.size(); i++) {
            int x = (int)random_engine() % width;
            int y = (int)random_engine() % height;
            agents[i]->set_goal(Position{x, y}, 0);
        }
    } else {
        throw std::invalid_argument("invalid goal type in GridWorld::set_goal");
    }
}

/**
 * reward description
 */
void GridWorld::define_agent_symbol(int no, int group, int index) {
    if (no >= agent_symbols.size()) {
        agent_symbols.resize((unsigned)no + 1);
    }
    agent_symbols[no].group = group;
    agent_symbols[no].index = index;
}

void GridWorld::define_event_node(int no, int op, int *inputs, int n_inputs) {
    if (no >= event_nodes.size()) {
        event_nodes.resize((unsigned)no + 1);
    }

    event_nodes[no].op = (EventOp)op; // be careful here
    for (int i = 0; i < n_inputs; i++)
        event_nodes[no].raw_parameter.push_back(inputs[i]);
}

void GridWorld::add_reward_rule(int on, int *receivers, float *values, int n_receiver, bool is_terminal) {
    RewardRule rule;

    rule.raw_parameter.push_back(on);
    for (int i = 0; i < n_receiver; i++) {
        rule.raw_parameter.push_back(receivers[i]);
        rule.values.push_back(values[i]);
    }
    rule.is_terminal = is_terminal;

    reward_rules.push_back(rule);
}

void GridWorld::collect_related_symbol(EventNode &node) {
    switch (node.op) {
        case OP_AND: case OP_OR:
            collect_related_symbol(*node.node_input[0]);
            collect_related_symbol(*node.node_input[1]);
            // union related symbols
            node.related_symbols.insert(node.node_input[0]->related_symbols.begin(), node.node_input[0]->related_symbols.end());
            node.related_symbols.insert(node.node_input[1]->related_symbols.begin(), node.node_input[1]->related_symbols.end());
            // union infer map
            node.infer_map.insert(node.node_input[0]->infer_map.begin(), node.node_input[0]->infer_map.end());
            node.infer_map.insert(node.node_input[1]->infer_map.begin(), node.node_input[1]->infer_map.end());
            break;
        case OP_NOT:
            collect_related_symbol(*node.node_input[0]);
            // union related symbols
            node.related_symbols.insert(node.node_input[0]->related_symbols.begin(), node.node_input[0]->related_symbols.end());
            // union infer map
            node.infer_map.insert(node.node_input[0]->infer_map.begin(), node.node_input[0]->infer_map.end());
            break;
        case OP_KILL: case OP_COLLIDE: case OP_ATTACK:
            node.related_symbols.insert(node.symbol_input[0]);
            node.related_symbols.insert(node.symbol_input[1]);
            break;
        case OP_AT: case OP_IN: case OP_DIE: case OP_IN_A_LINE:
            node.related_symbols.insert(node.symbol_input[0]);
            break;
    }
}

void GridWorld::init_reward_description() {
    // from serial data to pointer
    for (int i = 0; i < event_nodes.size(); i++) {
        EventNode &node = event_nodes[i];
        switch (node.op) {
            case OP_AND: case OP_OR:
                node.node_input.push_back(&event_nodes[node.raw_parameter[0]]);
                node.node_input.push_back(&event_nodes[node.raw_parameter[1]]);
                break;
            case OP_NOT:
                node.node_input.push_back(&event_nodes[node.raw_parameter[0]]);
                break;
            case OP_KILL: case OP_COLLIDE: case OP_ATTACK:
                node.symbol_input.push_back(&agent_symbols[node.raw_parameter[0]]);
                node.symbol_input.push_back(&agent_symbols[node.raw_parameter[1]]);
                node.infer_map.insert(std::make_pair(node.symbol_input[0], node.symbol_input[1]));
                break;
            case OP_AT:
                node.symbol_input.push_back(&agent_symbols[node.raw_parameter[0]]);
                node.int_input.push_back(node.raw_parameter[1]);
                node.int_input.push_back(node.raw_parameter[2]);
                break;
            case OP_IN:
                node.symbol_input.push_back(&agent_symbols[node.raw_parameter[0]]);
                node.int_input.push_back(node.raw_parameter[1]);
                node.int_input.push_back(node.raw_parameter[2]);
                node.int_input.push_back(node.raw_parameter[3]);
                node.int_input.push_back(node.raw_parameter[4]);
                break;
            case OP_DIE: case OP_IN_A_LINE:
                node.symbol_input.push_back(&agent_symbols[node.raw_parameter[0]]);
                break;
            default:
                throw std::invalid_argument("invalid op kind in event node");
        }
    }

    for (int i = 0; i < reward_rules.size(); i++) {
        RewardRule &rule = reward_rules[i];
        rule.on = &event_nodes[rule.raw_parameter[0]];
        for (int j = 1; j < rule.raw_parameter.size(); j++) {
            rule.receivers.push_back(&agent_symbols[rule.raw_parameter[j]]);
        }
    }

    // calc related symbols
    for (int i = 0; i < event_nodes.size(); i++) {
        collect_related_symbol(event_nodes[i]);
    }

    // for every reward rule, find all the input and build inference graph
    for (int i = 0; i < reward_rules.size(); i++) {
        std::vector<AgentSymbol*> input_symbols;
        std::vector<AgentSymbol*> infer_obj;
        std::set<AgentSymbol*> added;

        EventNode &on = *reward_rules[i].on;
        // first pass, scan to find infer pair, add them
        for (auto sub_iter = on.related_symbols.begin(); sub_iter != on.related_symbols.end();
            sub_iter++) {
            if (added.find(*sub_iter) != added.end()) // already be infered
                continue;

            auto obj_iter = on.infer_map.find(*sub_iter);
            if (obj_iter != on.infer_map.end()) { // can infer object
                input_symbols.push_back(*sub_iter);
                infer_obj.push_back(obj_iter->second);

                added.insert(*sub_iter);
                added.insert(obj_iter->second);
            }
        }

        // second pass, add remaining symbols
        for (auto sub_iter = on.related_symbols.begin(); sub_iter != on.related_symbols.end();
            sub_iter++) {
            if (added.find(*sub_iter) == added.end()) {
                input_symbols.push_back(*sub_iter);
                infer_obj.push_back(nullptr);
            }
        }

        reward_rules[i].input_symbols = input_symbols;
        reward_rules[i].infer_obj     = infer_obj;
    }

    /**
     * semantic check
     * 1. the object of attack, collide, kill cannot be a group
     * 2. any non-deterministic receiver must be involved in the triggering event
     */
    for (int i = 0; i < reward_rules.size(); i++) {
        // TODO: omitted temporally
    }

    // print rules for debug
    /*for (int i = 0; i < reward_rules.size(); i++) {
        printf("on: %d\n", (int)reward_rules[i].on->op);
        printf("input symbols: ");
        for (int j = 0; j < reward_rules[i].input_symbols.size(); j++) {
            printf("(%d,%d) ", reward_rules[i].input_symbols[j]->group,
                               reward_rules[i].input_symbols[j]->index);
            if (reward_rules[i].infer_obj[j] != nullptr) {
                printf("-> (%d,%d)", reward_rules[i].infer_obj[j]->group,
                                     reward_rules[i].infer_obj[j]->index);
            }
        }
        printf("\n");
    }*/
}

bool GridWorld::calc_event_node(EventNode *node) {
    bool ret;
    switch (node->op) {
        case OP_ATTACK: case OP_KILL: case OP_COLLIDE: {
                Agent *sub, *obj;
                // obj must be an agent, cannot be a group !
                assert(!node->symbol_input[1]->is_all());
                obj = (Agent *)node->symbol_input[1]->entity;
                if (node->symbol_input[0]->is_all()) {
                    const std::vector<Agent*> &agents = groups[node->symbol_input[0]->group].get_agents();
                    ret = true;
                    for (int i = 0; i < agents.size(); i++) {
                        sub = agents[i];

                        if (!(sub->get_last_op() == node->op && sub->get_op_obj() == obj)) {
                            ret = false;
                            break;
                        }
                    }
                } else {
                    sub = (Agent *)node->symbol_input[0]->entity;
                    ret = sub->get_last_op() == node->op && sub->get_op_obj() == obj;
                }
            }
            break;
        case OP_IN_A_LINE: {
                assert(node->symbol_input[0]->is_all());
                std::vector<Agent*> &agents = groups[node->symbol_input[0]->group].get_agents();
                if (agents.size() < 2) {
                    ret = true;
                } else {
                    ret = false;
                    // check if they are in a line, condition : 1. x is the same  2. max_y - min_y + 1 = #agent
                    int dx, dy;
                    dx = agents[0]->get_pos().x - agents[1]->get_pos().x;
                    dy = agents[0]->get_pos().y - agents[1]->get_pos().y;
                    bool in_line = true;
                    if (dx == 0 && dy != 0) {
                        int min_y, max_y;
                        int base_x = agents[0]->get_pos().x;
                        min_y = max_y = agents[0]->get_pos().y;
                        for (int i = 1; i < agents.size() && in_line; i++) {
                            Position pos = agents[i]->get_pos();
                            min_y = std::min(pos.y, min_y); max_y = std::max(pos.y, max_y);
                            in_line = (base_x == pos.x);
                        }
                        ret = in_line && max_y - min_y + 1 == agents.size();
                    } else if (dx != 0 && dy == 0) {
                        int min_x, max_x;
                        int base_y = agents[0]->get_pos().y;
                        min_x = max_x = agents[0]->get_pos().x;
                        for (int i = 1; i < agents.size() && in_line; i++) {
                            Position pos = agents[i]->get_pos();
                            min_x = std::min(pos.x, min_x); max_x = std::max(pos.x, max_x);
                            in_line = (base_y == pos.y);
                        }
                        ret = in_line && max_x - min_x + 1 == agents.size();
                    }
                }
            }
            break;
        case OP_AT: {
                std::vector<int> &int_input = node->int_input;
                if (node->symbol_input[0]->is_all()) {
                    const std::vector<Agent*> &agents = groups[node->symbol_input[0]->group].get_agents();
                    ret = true;
                    for (int i = 0; i < agents.size(); i++) {
                        Position pos = agents[i]->get_pos();
                        if (!(pos.x == int_input[0] && pos.y == int_input[1])) {
                            ret = false;
                            break;
                        }
                    }
                } else {
                    Agent *sub = (Agent *)node->symbol_input[0]->entity;
                    Position pos = sub->get_pos();
                    ret = (pos.x == int_input[0] && pos.y == int_input[1]);
                }
            }
            break;
        case OP_IN: {
                std::vector<int> &int_input = node->int_input;
                if (node->symbol_input[0]->is_all()) {
                    const std::vector<Agent*> &agents = groups[node->symbol_input[0]->group].get_agents();
                    ret = true;
                    for (int i = 0; i < agents.size(); i++) {
                        Position pos = agents[i]->get_pos();
                        if (!((pos.x > int_input[0] && pos.x < int_input[2]
                            && pos.y > int_input[1] && pos.y < int_input[3]))) {
                            ret = false;
                            break;
                        }
                    }
                } else {
                    Agent *sub = (Agent *)node->symbol_input[0]->entity;
                    Position pos = sub->get_pos();
                    ret = ((pos.x > int_input[0] && pos.x < int_input[2]
                          && pos.y > int_input[1] && pos.y < int_input[3]));
                }
            }
            break;
        case OP_DIE: {
                if (node->symbol_input[0]->is_all()) {
                    const std::vector<Agent*> &agents = groups[node->symbol_input[0]->group].get_agents();
                    ret = true;
                    for (int i = 0; i < agents.size(); i++) {
                        if (!agents[i]->is_dead()) {
                            ret = false;
                            break;
                        }
                    }
                } else {
                    Agent *sub = (Agent *)node->symbol_input[0]->entity;
                    ret = sub->is_dead();
                }
            }
            break;

        case OP_AND:
            ret = calc_event_node(node->node_input[0])
                && calc_event_node(node->node_input[1]);
            break;
        case OP_OR:
            ret = calc_event_node(node->node_input[0])
                || calc_event_node(node->node_input[1]);
            break;
        case OP_NOT:
            ret = !calc_event_node(node->node_input[0]);
            break;

        default:
            throw std::invalid_argument("invalid op of EventNode in GridWorld::calc_event_node");
    }

    return ret;
}

void GridWorld::calc_rule(std::vector<AgentSymbol *> &input_symbols,
                          std::vector<AgentSymbol *> &infer_obj,
                          RewardRule &rule, int now) {
    if (now == input_symbols.size()) {
        if (calc_event_node(rule.on)) { // if it is true, assign reward
            rule.trigger = true;
            const std::vector<AgentSymbol*> &receivers = rule.receivers;
            for (int i = 0; i < receivers.size(); i++) {
                AgentSymbol *sym = receivers[i];
                if (sym->is_all()) {
                    groups[sym->group].add_reward(rule.values[i]);
                } else {
                    Agent* agent = (Agent *)sym->entity;
                    agent->add_reward(rule.values[i]);
                }
            }
        }
    } else {
        AgentSymbol *sym = input_symbols[now];
        if (sym->is_any()) {
            const std::vector<Agent*> &agents = groups[sym->group].get_agents();
            for (int i = 0; i < agents.size(); i++) {
                sym->entity = (void *)agents[i];

                if (infer_obj[now] != nullptr) {   // can infer in this step
                    void *entity = agents[i]->get_op_obj();
                    if (entity != nullptr && infer_obj[now]->set_with_check(entity)) {
                        calc_rule(input_symbols, infer_obj, rule, now + 1);
                    }
                } else {
                    calc_rule(input_symbols, infer_obj, rule, now + 1);
                }
            }
        } else if (sym->is_all()) {
            if (infer_obj[now] != nullptr) {
                Group &g = groups[sym->group];
                if (g.get_agents().size() > 0)  {
                    void *entity = g.get_agents()[0]->get_op_obj(); // pick first agent to infer
                    if (entity != nullptr && infer_obj[now]->set_with_check(entity)) {
                        calc_rule(input_symbols, infer_obj, rule, now + 1);
                    }
                }
            } else {
                calc_rule(input_symbols, infer_obj, rule, now + 1);
            }
        } else { // deterministic ID
            Group &g = groups[sym->group];
            if (sym->index < g.get_size()) {
                Agent *agent = g.get_agents()[sym->index];
                sym->entity = (void *)agent;

                if (infer_obj[now] != nullptr) {
                    if (agent->get_op_obj() != nullptr) {
                        if (infer_obj[now]->set_with_check(agent->get_op_obj())) {
                            calc_rule(input_symbols, infer_obj, rule, now + 1);
                        }
                    }
                }
            }
        }
    }
}

void GridWorld::calc_reward() {
    size_t rule_size = reward_rules.size();
    for (int i = 0; i < rule_size; i++) {
        reward_rules[i].trigger = false;
        std::vector<AgentSymbol*> &input_symbols = reward_rules[i].input_symbols;
        std::vector<AgentSymbol*> &infer_obj     = reward_rules[i].infer_obj;
        calc_rule(input_symbols, infer_obj, reward_rules[i], 0);
    }
}

void GridWorld::get_reward(GroupHandle group, float *buffer) {
    std::vector<Agent*> &agents = groups[group].get_agents();

    size_t  agent_size = agents.size();
    Reward  group_reward = groups[group].get_reward();
    if (!goal_mode) {
        #pragma omp paralle for
        for (int i = 0; i < agent_size; i++) {
            buffer[i] = agents[i]->get_reward() + group_reward;
        }
    } else {
        int max_aside = std::max(width, height);
        #pragma omp parallel for
        for (int i = 0; i < agent_size; i++) {
            Position pos, goal;
            int radius;
            pos = agents[i]->get_pos();
            agents[i]->get_goal(goal, radius);
            int dis = std::max(abs(pos.x - goal.x), abs(pos.y - goal.y));
            dis = std::max(0, dis - radius);
            float goal_reward = 1.0f * (max_aside - dis) / max_aside;
            buffer[i] = agents[i]->get_reward() + group_reward + goal_reward;
        }
    }
}

/**
 * info getter
 */
void GridWorld::get_info(GroupHandle group, const char *name, void *void_buffer) {
    std::vector<Agent*> &agents = groups[group].get_agents();
    int   *int_buffer   = (int *)void_buffer;
    float *float_buffer = (float *)void_buffer;
    bool  *bool_buffer  = (bool *)void_buffer;

    DEBUG_PRINT("get info %s\n", *num);

    if (strequ(name, "num")) { // int
        int_buffer[0] = groups[group].get_num();
    } else if (strequ(name, "id")) {  // int
        size_t agent_size = agents.size();
        #pragma omp parallel for
        for (int i = 0; i < agent_size; i++) {
            int_buffer[i] = agents[i]->get_id();
        }
    } else if (strequ(name, "pos")) {   // int
        size_t agent_size = agents.size();
        #pragma omp parallel for
        for (int i = 0; i < agent_size; i++) {
            int_buffer[2 * i] = agents[i]->get_pos().x;
            int_buffer[2 * i + 1] = agents[i]->get_pos().y;
        }
    } else if (strequ(name, "alive")) {   // bool
        size_t agent_size = agents.size();
        #pragma omp parallel for
        for (int i = 0; i < agent_size; i++) {
            bool_buffer[i] = !agents[i]->is_dead();
        }
    } else if (strequ(name, "action_space")) {  // int
        int_buffer[0] = (int)groups[group].get_type().action_space.size();
    } else if (strequ(name, "view_space")) {     // int
        // the following is necessary! user can call get_view_space before reset
        groups[group].get_type().n_channel = group2channel((GroupHandle)groups.size());
        int_buffer[0] = groups[group].get_type().view_range->get_height();
        int_buffer[1] = groups[group].get_type().view_range->get_width();
        int_buffer[2] = groups[group].get_type().n_channel;
    } else if (strequ(name, "feature_space")) {
        int n_action = (int)groups[group].get_type().action_space.size();
        int feature_space = embedding_size + n_action;
        if (goal_mode)
            feature_space += 2;
        if (minimap_mode)  // x, y coordinate
            feature_space += 2;
        int_buffer[0] = feature_space;
    } else if (strequ(name, "view2attack")) {
        const AgentType &type = groups[group].get_type();
        const Range *range = type.attack_range;
        const Range *view_range = type.view_range;
        const int view_width = view_range->get_width(), view_height = view_range->get_height();

        int (*buf)[view_width];
        buf = (int (*)[view_width]) int_buffer;

        memset(buf, -1, sizeof(int) * view_height * view_width);
        int x1, y1, x2, y2;

        view_range->get_range_rela_offset(x1, y1, x2, y2);
        for (int i = 0; i < range->get_count(); i++) {
            int dx, dy;
            range->num2delta(i, dx, dy);
            //dx -= type.att_x_offset; dy -= type.att_y_offset;

            buf[dy - y1][dx - x1] = i;
        }
    } else if (strequ(name, "attack_base")) {
        int_buffer[0] = groups[group].get_type().attack_base;
    } else if (strequ(name, "both_attack")) {
        int_buffer[0] = stat_recorder.both_attack;
    } else {
        throw std::runtime_error("unsupported info name in GridWorld::get_info");
    }
}

/**
 * render
 */
void GridWorld::render() {
    if (render_generator.get_save_dir() == "___debug___")
        map.render();
    else {
        if (first_render) {
            first_render = false;
            render_generator.gen_config(groups, map, width, height);
        }
        render_generator.render_a_frame(groups);
    }
}


/*** AGENTS ***/
#define AGENT_TYPE_SET_INT(name) \
    if (strequ(keys[i], #name)) {\
        name = (int)(values[i] + 0.5);\
        is_set = true;\
    }

#define AGENT_TYPE_SET_FLOAT(name) \
    if (strequ(keys[i], #name)) {\
        name = values[i];\
        is_set = true;\
    }

#define AGENT_TYPE_SET_BOOL(name)\
    if (strequ(keys[i], #name)) {\
        name = bool(int(values[i] + 0.5));\
        is_set = true;\
    }

AgentType::AgentType(int n, std::string name, const char **keys, float *values, bool turn_mode) {
    this->name = name;

    // default value
    attack_in_group = false;
    width = length = 1;
    hp = 1.0;
    damage = eat_ability = 0;
    food_supply = kill_supply = 0.0;
    step_reward = kill_reward  = dead_penalty = attack_penalty = 0.0;
    view_radius = 1; view_angle = 360;

    // init member vars from str
    bool is_set;
    for (int i = 0; i < n; i++) {
        is_set = false;
        AGENT_TYPE_SET_INT(width);
        AGENT_TYPE_SET_INT(length);

        AGENT_TYPE_SET_FLOAT(speed);
        AGENT_TYPE_SET_FLOAT(hp);

        AGENT_TYPE_SET_FLOAT(view_radius);  AGENT_TYPE_SET_FLOAT(view_angle);
        AGENT_TYPE_SET_FLOAT(attack_radius);AGENT_TYPE_SET_FLOAT(attack_angle);

        AGENT_TYPE_SET_FLOAT(hear_radius);  AGENT_TYPE_SET_FLOAT(speak_radius);
        AGENT_TYPE_SET_INT(speak_ability);

        AGENT_TYPE_SET_FLOAT(damage);       AGENT_TYPE_SET_FLOAT(trace);
        AGENT_TYPE_SET_FLOAT(eat_ability);
        AGENT_TYPE_SET_FLOAT(step_recover); AGENT_TYPE_SET_FLOAT(kill_supply);
        AGENT_TYPE_SET_FLOAT(food_supply);

        AGENT_TYPE_SET_BOOL(attack_in_group);

        AGENT_TYPE_SET_FLOAT(step_reward);  AGENT_TYPE_SET_FLOAT(kill_reward);
        AGENT_TYPE_SET_FLOAT(dead_penalty); AGENT_TYPE_SET_FLOAT(attack_penalty);

        AGENT_TYPE_SET_FLOAT(view_x_offset); AGENT_TYPE_SET_FLOAT(view_y_offset);
        AGENT_TYPE_SET_FLOAT(att_x_offset);  AGENT_TYPE_SET_FLOAT(att_y_offset);
        AGENT_TYPE_SET_FLOAT(turn_x_offset); AGENT_TYPE_SET_FLOAT(turn_y_offset);

        if (!is_set) {
            throw std::runtime_error("invalid agent config");
        }
    }

    // FIXME: do not support SectorRange with angle >= 180, only support circle range when angle >= 180
    int parity = width % 2; // use parity to make range center-symmetric
    if (view_angle >= 180) {
        if (fabs(view_angle - 360) > 1e-5) {
            throw std::invalid_argument("only supports angle = 360 when angle > 180.");
        }
        view_range = new CircleRange(view_radius, 0, parity);
    } else {
        view_range = new SectorRange(view_angle, view_radius, parity);
    }

    if (attack_angle >= 180) {
        if (fabs(attack_angle - 360) > 1e-5) {
            throw std::invalid_argument("only supports angle = 360 when angle > 180.");
        }
        attack_range = new CircleRange(attack_radius, width / 2.0f, parity);
    } else {
        attack_range = new SectorRange(attack_angle, attack_radius, parity);
    }

    move_range   = new CircleRange(speed, 0, 1);
    view_x_offset = width / 2; view_y_offset = length / 2;
    att_x_offset  = width / 2; att_y_offset  = length / 2;
    turn_x_offset = 0; turn_y_offset = 0;

    move_base = 0;
    turn_base = move_range->get_count();

    if (turn_mode) {
        attack_base = turn_base + 2;
    } else {
        attack_base = turn_base;
    }
    int n_action = attack_base + attack_range->get_count();
    for (int i = 0; i < n_action; i++) {
        action_space.push_back(i);
    }
    // action space layout : move turn attack ...
}

} // namespace magent
} // namespace gridworld
