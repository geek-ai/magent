//
// Created by mercy on 17-10-6.
//

#include "reward_description.h"
#include "GridWorld.h"

namespace magent {
namespace gridworld {

bool AgentSymbol::set_with_check(void *entity) {
    Agent *agent = (Agent *)entity;
    if (group != agent->get_group())
        return false;
    if (index != -1 && index != agent->get_index())
        return false;
    this->entity = agent;
    return true;
}

} // namespace gridworld
} // namespace magent