//
// Created by mercy on 17-7-26.
//

#include <cstring>
#include "utility.h"

namespace magent {
namespace utility {

bool strequ(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

} // utility
} // magent

