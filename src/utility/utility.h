//
// Created by mercy on 17-7-26.
//

#ifndef MAGNET_UTILITY_H
#define MAGNET_UTILITY_H

namespace magent {
namespace utility {

bool strequ(const char *a, const char *b);

} // namespace utilty
} // namespace magent

#endif //MAGNET_UTILITY_H
