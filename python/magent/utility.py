import math
import collections
import numpy as np

from magent.builtin.rule_model import RandomActor

"""
Episode buffer for replay
"""
class EpisodesBufferEntry:
    def __init__(self):
        self.views = []
        self.features = []
        self.actions = []
        self.rewards = []
        self.terminal = False

    def append(self, view, feature, action, reward, alive):
        self.views.append(view.copy())
        self.features.append(feature.copy())
        self.actions.append(action)
        self.rewards.append(reward)
        if not alive:
            self.terminal = True


class EpisodesBuffer:
    def __init__(self, capacity):
        self.buffer = {}
        self.capacity = capacity
        self.is_full = False

    def record_step(self, ids, obs, acts, rewards, alives):
        buffer = self.buffer
        index = np.random.permutation(len(ids))
        for i in xrange(len(ids)):
            i = index[i]
            entry = buffer.get(ids[i])
            if entry is None:
                if self.is_full:
                    continue
                else:
                    entry = EpisodesBufferEntry()
                    buffer[ids[i]] = entry
                    if len(buffer) >= self.capacity:
                        self.is_full = True

            entry.append(obs[0][i], obs[1][i], acts[i], rewards[i], alives[i])

    def reset(self):
        self.buffer = {}
        self.is_full = False

    def get_dict_buf(self):
        return self.buffer


class EpisodesBuffersQueue:
    def __init__(self, len):
        self.queue = collections.deque(maxlen=len)

    def append(self, item):
        self.queue.append(item)

    def get_buffers(self):
        return self.queue

"""
Client to host a model
"""
def model_client(conn, env, handle, RLModel, args, queue_length, buf_capacity):
    model = RLModel(env, handle, **args)
    replay_queue = EpisodesBuffersQueue(len=queue_length)
    sample_buffer = EpisodesBuffer(capacity=buf_capacity)

    while True:
        cmd = conn.recv()
        if cmd[0] == 'act':
            obs = cmd[1]
            ids = cmd[2]
            policy = cmd[3]
            eps = cmd[4]
            acts = model.infer_action(obs, ids, policy=policy, eps=eps)
            conn.send(acts)
        elif cmd[0] == 'train':
            print_every = cmd[1]
            replay_queue.append(sample_buffer)
            total_loss, value = model.train(replay_queue, print_every=print_every)
            sample_buffer = EpisodesBuffer(buf_capacity)
            conn.send((total_loss, value))
        elif cmd[0] == 'sample':
            rewards = cmd[1]
            alives = cmd[2]
            sample_buffer.record_step(ids, obs, acts, rewards, alives)
            conn.send("done")
        elif cmd[0] == 'save':
            savedir = cmd[1]
            n_iter = cmd[2]
            model.save(savedir, n_iter)
            conn.send("done")
        elif cmd[0] == 'load':
            savedir = cmd[1]
            n_iter = cmd[2]
            model.load(savedir, n_iter)
            conn.send("done")
        elif cmd[0] == 'quit':
            break
        else:
            print("Error: Unknown command %s" % cmd[0])
            break


"""
decay schedulers
"""

def exponential_decay(now_step, total_step, final_value, rate):
    decay = math.exp(math.log(final_value)/total_step ** rate)
    return max(final_value, 1 * decay ** (now_step ** rate))


def linear_decay(now_step, total_step, final_value):
    decay = (1 - final_value) / total_step
    return max(final_value, 1 - decay * now_step)


def piecewise_decay(now_step, anchor, anchor_value):
    i = 0
    while i < len(anchor) and now_step >= anchor[i]:
        i += 1

    if i == len(anchor):
        return anchor_value[-1]
    else:
        return anchor_value[i-1] + (now_step - anchor[i-1]) * \
                                   ((anchor_value[i] - anchor_value[i-1]) / (anchor[i] - anchor[i-1]))

"""
eval observation set generator
"""
def sample_observation(env, handles, sample_id, n=-1, step=-1):
    models = [RandomActor(env, handle) for handle in handles]
    views = np.empty((0,) + env.get_view_space(handles[sample_id]))
    features = np.empty((0,) + env.get_feature_space(handles[sample_id]))

    done = False
    step_ct = 0
    while not done:
        obs = [env.get_observation(handle) for handle in handles]
        ids = [env.get_agent_id(handle) for handle in handles]

        for i in range(len(handles)):
            act = models[i].infer_action(obs[i], ids[i])
            env.set_action(handles[i], act)

        done = env.step()
        env.clear_dead()

        views = np.concatenate((views, obs[sample_id][0]))
        features = np.concatenate((features, obs[sample_id][1]))

        if step != -1 and step_ct > step:
            break

        if step_ct % 50 == 0:
            print("sample %d" % step_ct)

        step_ct += 1

    if n != -1:
        views = views[np.random.choice(np.arange(views.shape[0]), n)]
        features = features[np.random.choice(np.arange(features.shape[0]), n)]

    return views, features


