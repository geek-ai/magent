# coding: utf-8
from __future__ import absolute_import

import os
import ctypes


def _load_lib():
    """ Load library in build/lib. """
    cur_path = os.path.dirname(os.path.abspath(os.path.expanduser(__file__)))
    lib_path = os.path.join(cur_path, "../../build/")
    path_to_so_file = os.path.join(lib_path, "libmagent.so")
    lib = ctypes.CDLL(path_to_so_file, ctypes.RTLD_GLOBAL)
    return lib


def as_float_c_array(buf):
    return buf.ctypes.data_as(ctypes.POINTER(ctypes.c_float))


def as_int32_c_array(buf):
    return buf.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))


def as_bool_c_array(buf):
    return buf.ctypes.data_as(ctypes.POINTER(ctypes.c_bool))

_LIB = _load_lib()
