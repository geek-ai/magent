import os
import time

import numpy as np
import mxnet as mx

from magent.model import BaseModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue


class DeepQNetwork(BaseModel):
    def __init__(self, env, handle, name, eval_obs=None,
                 batch_size=64, reward_decay=0.99, learning_rate=1e-4,
                 train_freq=1, target_freq=2000,
                 use_dueling=True, use_double=True, use_prior_sample=False):
        BaseModel.__init__(self, env, handle)
        env.nchw_layout = True   # set the layout of input
        # ======================== set config  ========================
        self.env = env
        self.handle = handle
        self.name = name
        self.view_space = env.get_view_space(handle)
        self.feature_space = env.get_feature_space(handle)
        self.num_actions  = env.get_action_space(handle)[0]

        self.batch_size   = batch_size
        self.learning_rate= learning_rate
        self.train_freq   = train_freq   # train time of every sample (s,a,r,s')
        self.target_freq  = target_freq  # update frequency of target network
        self.eval_obs     = eval_obs

        self.use_dueling  = use_dueling
        self.use_double   = use_double
        self.use_prior_sample = use_prior_sample

        self.train_ct = 0

        # ======================= build network =======================
        self.ctx = mx.cpu()

        self.input_view = mx.sym.var("input_view")
        self.input_feature = mx.sym.var("input_feature")
        self.action = mx.sym.var("action")
        self.target = mx.sym.var("target")

        self.qvalues = self._create_network(self.input_view, self.input_feature)
        self.gamma = reward_decay
        self.action_onehot = mx.sym.one_hot(self.action, depth=self.num_actions)
        self.loss = mx.sym.mean(
            mx.sym.square(
                self.target - mx.sym.sum(self.qvalues * self.action_onehot, axis=1)
            ))
        self.loss = mx.sym.MakeLoss(data=self.loss)

        self.out_qvalues = mx.sym.BlockGrad(self.qvalues)
        self.output = mx.sym.Group([self.out_qvalues, self.loss])

        self.model = mx.mod.Module(self.output,
                                   data_names=['input_view', 'input_feature'],
                                   label_names=['action', 'target'], context=self.ctx)

        self.target_model = mx.mod.Module(self.qvalues,
                                          data_names=['input_view', 'input_feature'],
                                          label_names=[], context=self.ctx)

        # bind (set initial batch size)
        self.bind_size = batch_size
        self.model.bind(data_shapes=[('input_view', (batch_size,) + self.view_space),
                                     ('input_feature', (batch_size,) + self.feature_space)],
                        label_shapes=[('action', (batch_size,)),
                                      ('target', (batch_size,))])
        self.target_model.bind(data_shapes=[('input_view', (batch_size,) + self.view_space),
                                            ('input_feature', (batch_size,) + self.feature_space)])

        # init params
        self.model.init_params(initializer=mx.init.Xavier())
        self.model.init_optimizer(optimizer='adam', optimizer_params={
                                  'learning_rate': learning_rate})

        self._copy_network(self.target_model, self.model)

        # init training buffers
        self.train_ct = 0

        self.view_buf = np.empty((1,) + self.view_space)
        self.feature_buf = np.empty((1,) + self.feature_space)
        self.action_buf, self.reward_buf = np.empty(1, dtype=np.int32), np.empty(1)
        self.terminal_buf = np.empty(1, dtype=np.bool)

        # print("parameters", self.model.get_params())
        # mx.viz.plot_network(self.loss).view()

    def _create_network(self, input_view, input_feature):
        kernel_num = [32, 32]
        emb_size = 32
        hidden_size = [256 - emb_size]

        conv1 = mx.sym.Convolution(data=input_view, kernel=(3, 3), num_filter=kernel_num[0])
        conv1 = mx.sym.Activation(data=conv1, act_type="relu")

        conv2 = mx.sym.Convolution(data=conv1, kernel=(3, 3), num_filter=kernel_num[1])
        conv2 = mx.sym.Activation(data=conv2, act_type="relu")

        flat_view = mx.sym.flatten(data=conv2)

        embedding = mx.sym.FullyConnected(data=input_feature, num_hidden=emb_size)
        embedding = mx.sym.Activation(data=embedding, act_type="relu")

        all_flatten = mx.sym.concat(flat_view, embedding, dim=1)

        dense = mx.sym.FullyConnected(data=all_flatten, num_hidden=hidden_size[0])
        dense = mx.sym.Activation(data=dense, act_type="relu")

        if self.use_dueling:
            # state value
            value = mx.sym.FullyConnected(data=dense, num_hidden=1)
            advantage = mx.sym.FullyConnected(data=dense, num_hidden=self.num_actions)

            mean = mx.sym.mean(advantage, axis=1, keepdims=True)
            advantage = mx.sym.broadcast_add(advantage, mean)
            qvalues = mx.sym.broadcast_add(advantage, value)
        else:
            qvalues = mx.sym.FullyConnected(data=dense, num_hidden=self.num_actions)

        return qvalues

    def infer_action(self, raw_obs, ids, policy="e_greedy", eps=0):
        view, feature = raw_obs[0], raw_obs[1]
        n = len(view)

        self._reset_bind_size(n)
        with self.ctx:
            data_batch = mx.io.DataBatch(data=[mx.nd.array(view), mx.nd.array(feature)])
            self.model.forward(data_batch, is_train=False)
            qvalue_batch = self.model.get_outputs()[0]
            best_actions = mx.nd.argmax(qvalue_batch, axis=1).asnumpy()

        if policy == "e_greedy":
            random = np.random.randint(self.num_actions, size=(n,))
            cond = np.random.uniform(0, 1, size=(n,)) < eps
            ret = np.where(cond, random, best_actions)
        elif policy == 'greedy':
            ret = best_actions

        return ret.astype(np.int32)

    def _calc_target(self, next_view, next_feature, rewards, terminal):
        n = len(rewards)
        data_batch = mx.io.DataBatch(data=[mx.nd.array(next_view), mx.nd.array(next_feature)])

        with self.ctx:
            if self.use_double:
                self.target_model.forward(data_batch, is_train=False)
                self.model.forward(data_batch, is_train=False)
                t_qvalues = self.target_model.get_outputs()[0].asnumpy()
                qvalues   = self.model.get_outputs()[0].asnumpy()
                next_value = t_qvalues[np.arange(n), np.argmax(qvalues, axis=1)]
            else:
                self.target_model.forward(data_batch, is_train=False)
                t_qvalues = self.target_model.get_outputs()[0]
                next_value = np.max(t_qvalues, axis=1)

        target = np.where(terminal, rewards, rewards + self.gamma * next_value)

        return target

    def train(self, sample_buffers, print_every=1000):
        batch_size = self.batch_size
        self._reset_bind_size(batch_size)
        total_loss = 0

        # deal with buffer type
        if isinstance(sample_buffers, EpisodesBuffersQueue):  # a queue of buffer
            buffers = sample_buffers.get_buffers()
        elif isinstance(sample_buffers, EpisodesBuffer):  # a single buffer
            buffers = [sample_buffers]  # for consistent, make it iterable
        else:
            raise RuntimeError("unsupported buffer type")

        # calc buffer size
        n = non_terminal = 0
        pad = 1  # 1 is for padding the last frame
        for sample_buffer in buffers:
            for episode in sample_buffer.get_dict_buf().values():
                n += len(episode.rewards)
                if not episode.terminal:
                    non_terminal += 1

        # resize to the new size
        self.view_buf.resize((n + pad,) + self.view_space)
        self.feature_buf.resize((n + pad,) + self.feature_space)
        self.action_buf.resize(n+pad)
        self.reward_buf.resize(n + pad)
        self.terminal_buf.resize(n + pad)
        view, feature = self.view_buf, self.feature_buf
        action, reward = self.action_buf, self.reward_buf
        terminal = self.terminal_buf

        head = np.empty(n - non_terminal, dtype=np.int)
        ct = p_head = 0

        # collect episodes from multiple separate buffers to a continuous buffer
        for sample_buffer in buffers:
            for id, episode in sample_buffer.get_dict_buf().items():
                v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards
                m = len(episode.rewards)

                view[ct:ct+m] = v
                feature[ct:ct+m] = f
                action[ct:ct+m]  = a
                reward[ct:ct+m]  = r
                terminal[ct:ct+m] = False

                if episode.terminal:
                    head[p_head:p_head+m] = np.arange(ct, ct + m)
                    p_head += m
                    terminal[ct+m-1] = True
                else:  # if it is not terminal, then the last frame is only for calculating target, not for training
                    head[p_head:p_head+m-1] = np.arange(ct, ct + m - 1)
                    p_head += m-1

                ct += m
        assert ct == n + pad - 1 and p_head == n - non_terminal

        # for shuffle
        np.random.shuffle(head)

        # train batches
        n_batches = int(self.train_freq * n / batch_size)
        print("batch number %d" % n_batches)

        ct = pt = 0
        start_time = time.time()
        for i in range(n_batches):
            with self.ctx:
                self._reset_bind_size(batch_size)
                pick = head[pt:pt + batch_size]
                target = self._calc_target(view[pick+1], feature[pick+1],
                                           reward[pick], terminal[pick])
                batch = mx.io.DataBatch(data=[mx.nd.array(view[pick]),
                                              mx.nd.array(feature[pick])],
                                        label=[mx.nd.array(action[pick]),
                                               mx.nd.array(target)])

                self.model.forward(batch, is_train=True)
                self.model.backward()
                loss = self.model.get_outputs()[1].asnumpy()
                self.model.update()

            if self.train_ct % self.target_freq == 0:
                self._copy_network(self.target_model, self.model)

            if ct % print_every == 0:
                print("batch %5d,  loss %.6f,  eval %.6f" % (ct, loss, self._eval(target)))

            total_loss += loss
            ct += 1
            pt = (pt + batch_size) % (n - non_terminal)
            pt = pt if pt + batch_size < (n - non_terminal) else 0
            self.train_ct += 1

        total_time = time.time() - start_time
        step_average = total_time / max(1.0, (ct / 1000.0))
        print("batches: %d, total time: %.2f  1k average: %.2f" % (ct, total_time, step_average))

        return total_loss / ct if ct != 0 else 0, self._eval(target)

    def _reset_bind_size(self, new_size):
        if self.bind_size == new_size:
            return
        else:
            self.bind_size = new_size
            self.model.reshape(
                data_shapes=[
                    ('input_view',    (new_size,) + self.view_space),
                    ('input_feature', (new_size,) + self.feature_space)],
                label_shapes=[
                    ('action',        (new_size,)),
                    ('target',        (new_size,))
                ]
            )

    def _copy_network(self, dest, source):
        arg_params, aux_params = source.get_params()
        dest.set_params(arg_params, aux_params)

    def _eval(self, target):
        if self.eval_obs is None:
            return np.mean(target)
        else:
            self._reset_bind_size(len(self.eval_obs[0]))
            with self.ctx:
                batch = mx.io.DataBatch(data=[mx.nd.array(self.eval_obs[0]),
                                              mx.nd.array(self.eval_obs[1])])
                self.model.forward(batch, is_train=True)
                return np.mean(self.model.get_outputs()[0].asnumpy())

    def save(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name, )
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        pre = os.path.join(dir_name, "mxdqn")
        self.model.save_checkpoint(pre, epoch, save_optimizer_states=True)

    def load(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        pre = os.path.join(dir_name, "mxdqn")
        _, arg_params, aux_params = mx.model.load_checkpoint(pre, epoch)
        self.model.set_params(arg_params, aux_params, force_init=True)

    def get_info(self):
        return "mx dqn train_time: %d" % (self.train_ct)
