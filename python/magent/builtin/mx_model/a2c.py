import os
import time

import numpy as np
import mxnet as mx

from magent.model import BaseModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue


class AdvantageActorCritic(BaseModel):
    def __init__(self, env, handle, name, eval_obs=None,
                 batch_size=64, reward_decay=0.99, learning_rate=1e-3,
                 train_freq=1, value_coef=0.1, ent_coef=0.01):
        BaseModel.__init__(self, env, handle)
        env.nchw_layout = True
        # ======================== set config  ========================
        self.env = env
        self.handle = handle
        self.name = name
        self.view_space = env.get_view_space(handle)
        self.feature_space = env.get_feature_space(handle)
        self.num_actions  = env.get_action_space(handle)[0]
        self.reward_decay = reward_decay

        self.batch_size   = batch_size
        self.learning_rate= learning_rate
        self.train_freq   = train_freq   # train time of every sample (s,a,r,s')
        self.eval_obs     = eval_obs

        self.value_coef = value_coef
        self.ent_coef   = ent_coef

        self.train_ct = 0

        # ======================= build network =======================
        self.ctx = mx.cpu()

        self.input_view = mx.sym.var("input_view")
        self.input_feature = mx.sym.var("input_feature")

        policy, value = self._create_network(self.input_view, self.input_feature)

        log_policy = mx.sym.log(policy)
        out_policy = mx.sym.BlockGrad(policy)

        neg_entropy = policy * log_policy
        neg_entropy = mx.sym.MakeLoss(
            data=neg_entropy, grad_scale=ent_coef
        )

        self.output = mx.sym.Group([log_policy, value, neg_entropy, out_policy])
        self.model = mx.mod.Module(self.output, data_names=['input_view', 'input_feature'],
                                   label_names=None, context=self.ctx)

        # bind (set initial batch size)
        self.bind_size = batch_size
        self.model.bind(data_shapes=[('input_view', (batch_size,) + self.view_space),
                                     ('input_feature', (batch_size,) + self.feature_space)],
                        label_shapes=None)

        # init params
        self.model.init_params(initializer=mx.init.Xavier())
        self.model.init_optimizer(optimizer='adam', optimizer_params={
            'learning_rate': learning_rate,
            'clip_gradient': 10,
        })

        # init training buffers

        self.view_buf = np.empty((1,) + self.view_space)
        self.feature_buf = np.empty((1,) + self.feature_space)
        self.action_buf = np.empty(1, dtype=np.int32)
        self.advantage_buf, self.value_buf = np.empty(1), np.empty(1)
        self.terminal_buf = np.empty(1, dtype=np.bool)

        # print("parameters", self.model.get_params())
        # mx.viz.plot_network(self.output).view()

    def _create_network(self, input_view, input_feature):
        kernel_num = [16, 16]
        hidden_size = [128 - 16]
        emb_size = 16

        conv1 = mx.sym.Convolution(data=input_view, kernel=(3, 3), num_filter=kernel_num[0])
        conv1 = mx.sym.Activation(data=conv1, act_type="relu")

        conv2 = mx.sym.Convolution(data=conv1, kernel=(3, 3), num_filter=kernel_num[1])
        conv2 = mx.sym.Activation(data=conv2, act_type="relu")

        flat_view = mx.sym.flatten(data=conv2)

        embedding = mx.sym.FullyConnected(data=input_feature, num_hidden=emb_size)
        embedding = mx.sym.Activation(data=embedding, act_type="relu")

        all_flatten = mx.sym.concat(flat_view, embedding, dim=1)

        dense = mx.sym.FullyConnected(data=all_flatten, num_hidden=hidden_size[0], no_bias=True)
        dense = mx.sym.Activation(data=dense, act_type="relu")

        policy = mx.sym.FullyConnected(data=dense, num_hidden=self.num_actions, no_bias=True)
        policy = mx.sym.SoftmaxActivation(data=policy)
        policy = mx.sym.clip(data=policy, a_min=1e-5, a_max=1 - 1e-5)
        value  = mx.sym.FullyConnected(data=dense, num_hidden=1)

        return policy, value

    def infer_action(self, raw_obs, ids, policy="e_greedy", eps=0):
        view, feature = raw_obs[0], raw_obs[1]
        n = len(view)

        ret = np.empty(n, dtype=np.int32)
        with self.ctx:
            self._reset_bind_size(n)
            data_batch = mx.io.DataBatch(data=[mx.nd.array(view), mx.nd.array(feature)])
            self.model.forward(data_batch, is_train=False)
            policy = self.model.get_outputs()[3].asnumpy()

            actions = np.arange(self.num_actions)
            for i in xrange(n):
                ret[i] = np.random.choice(actions, p=policy[i])

        return ret

    def train(self, sample_buffers, print_every=1000):
        # deal with buffer type
        if isinstance(sample_buffers, EpisodesBuffersQueue):  # a queue of buffer
            buffers = sample_buffers.get_buffers()
        elif isinstance(sample_buffers, EpisodesBuffer):  # a single buffer
            buffers = [sample_buffers]  # for consistent, make it iterable
        else:
            raise RuntimeError("unsupported buffer type")

        # calc buffer size
        n = 0
        for sample_buffer in buffers:
            for episode in sample_buffer.get_dict_buf().values():
                if episode.terminal:
                    n += len(episode.rewards)
                else:
                    n += len(episode.rewards) - 1

        # resize to the new size
        self.view_buf.resize((n,) + self.view_space)
        self.feature_buf.resize((n,) + self.feature_space)
        self.action_buf.resize(n)
        self.value_buf.resize(n)
        self.advantage_buf.resize(n)
        view, feature = self.view_buf, self.feature_buf
        action, value = self.action_buf, self.value_buf
        advantage     = self.advantage_buf

        ct = 0
        gamma = self.reward_decay
        # collect episodes from multiple separate buffers to a continuous buffer
        with self.ctx:
            for sample_buffer in buffers:
                for id, episode in sample_buffer.get_dict_buf().iteritems():
                    v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards

                    m = len(episode.rewards)
                    self._reset_bind_size(m)
                    data_batch = mx.io.DataBatch(data=[mx.nd.array(v), mx.nd.array(f)])
                    self.model.forward(data_batch, is_train=False)
                    value = self.model.get_outputs()[1].asnumpy()
                    value = value.reshape((len(value),))

                    delta_t = np.empty(m)
                    if episode.terminal:
                        r = np.array(r)
                        delta_t[:m-1] = r[:m-1] + gamma * value[1:m] - value[:m-1]
                        delta_t[m-1]  = r[m-1]  + gamma * 0          - value[m-1]
                    else:
                        delta_t[:m-1] = r[:m-1] + gamma * value[1:m] - value[:m-1]
                        m -= 1
                        v, f, a = v[:-1], f[:-1], a[:-1]

                    # discount advantage
                    keep = 0
                    for i in reversed(range(m)):
                        keep = keep * gamma + delta_t[i]
                        advantage[ct+i] = keep

                    view[ct:ct+m] = v
                    feature[ct:ct+m] = f
                    action[ct:ct+m]  = a
                    ct += m
        assert n == ct

        if n == 0:
            return [0, 0, 0], 0

        with self.ctx:
            n = len(advantage)
            advantage = -advantage
            neg_advantage = advantage

            neg_advs_np = np.zeros((n, self.num_actions), dtype=np.float32)
            neg_advs_np[np.arange(n), action] = neg_advantage
            neg_advs = mx.nd.array(neg_advs_np)

            # the grads of values are exactly negative advantages
            v_grads = mx.nd.array(self.value_coef * (neg_advantage[:, np.newaxis]))
            self._reset_bind_size(n)
            data_batch = mx.io.DataBatch(data=[mx.nd.array(view), mx.nd.array(feature)])
            self.model.forward(data_batch, is_train=True)
            _, value, entropy_loss, _ = self.model.get_outputs()
            self.model.backward(out_grads=[neg_advs, v_grads])
            self.model.update()

            value = mx.nd.mean(value).asnumpy()[0]
            pg_loss = np.mean(advantage)
            entropy_loss = self.ent_coef * mx.nd.mean(entropy_loss).asnumpy()[0]
            value_loss = self.value_coef * np.mean(advantage) ** 2

        return [pg_loss, value_loss, entropy_loss], value

    def _reset_bind_size(self, new_size):
        if self.bind_size == new_size:
            return
        else:
            self.bind_size = new_size
            self.model.reshape(
                data_shapes=[
                    ('input_view',    (new_size,) + self.view_space),
                    ('input_feature', (new_size,) + self.feature_space)],
            )

    def save(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name, )
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        pre = os.path.join(dir_name, "mxdqn")
        self.model.save_checkpoint(pre, epoch, save_optimizer_states=True)

    def load(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        pre = os.path.join(dir_name, "mxdqn")
        _, arg_params, aux_params = mx.model.load_checkpoint(pre, epoch)
        self.model.set_params(arg_params, aux_params, force_init=True)

    def get_info(self):
        return "mx dqn train_time: %d" % (self.train_ct)
