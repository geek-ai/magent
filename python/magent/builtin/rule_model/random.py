import numpy as np

from magent.model import BaseModel


class RandomActor(BaseModel):
    def __init__(self, env, handle, *args, **kwargs):
        BaseModel.__init__(self, env, handle)

        self.env = env
        self.handle = handle
        self.n_action = env.get_action_space(handle)[0]

    def infer_action(self, *args, **kwargs):
        num = self.env.get_num(self.handle)
        actions = np.random.randint(self.n_action, size=num, dtype=np.int32)
        return actions

