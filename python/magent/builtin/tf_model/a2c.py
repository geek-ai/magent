import time
import os

import numpy as np
import tensorflow as tf

from magent.model import BaseModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue


def sample(logits):
    noise = tf.random_uniform(tf.shape(logits))
    return tf.argmax(logits - tf.log(-tf.log(noise)), 1)


def cat_entropy(logits):
    a0 = logits - tf.reduce_max(logits, 1, keep_dims=True)
    ea0 = tf.exp(a0)
    z0 = tf.reduce_sum(ea0, 1, keep_dims=True)
    p0 = ea0 / z0
    return tf.reduce_sum(p0 * (tf.log(z0) - a0), 1)


class AdvantageActorCritic(BaseModel):
    def __init__(self, env, handle, name, eval_obs=None,
                 batch_size=64, reward_decay=0.99, learning_rate=1e-3,
                 train_freq=1, state_coef=0.1, ent_coef=0.01):
        BaseModel.__init__(self, env, handle)
        # ======================== set config  ========================
        self.env = env
        self.handle = handle
        self.name = name
        self.view_space = env.get_view_space(handle)
        self.feature_space = env.get_feature_space(handle)
        self.num_actions  = env.get_action_space(handle)[0]
        self.reward_decay = reward_decay

        self.batch_size   = batch_size
        self.learning_rate= learning_rate
        self.train_freq   = train_freq   # train time of every sample (s,a,r,s')

        self.ent_coef = ent_coef         # coefficient of entropy in the total loss
        self.state_coef = state_coef   # coefficient of state value in the total loss

        self.train_ct = 0

        # ======================= build network =======================
        with tf.name_scope(self.name):
            self._create_network(self.view_space, self.feature_space)

        # init tensorflow session
        config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.sess.run(tf.global_variables_initializer())

        self.view_buf = np.empty((1,) + self.view_space)
        self.feature_buf = np.empty((1,) + self.feature_space)
        self.action_buf, self.reward_buf = np.empty(1, dtype=np.int32), np.empty(1)
        self.advantage_buf, self.value_buf = np.empty(1), np.empty(1)
        self.terminal_buf = np.empty(1, dtype=np.bool)

    def _create_network(self, view_space, feature_space):
        # input
        input_view    = tf.placeholder(tf.float32, (None,) + self.view_space)
        input_feature = tf.placeholder(tf.float32, (None,) + self.feature_space)
        action = tf.placeholder(tf.int32, [None])
        reward = tf.placeholder(tf.float32, [None])
        advantage = tf.placeholder(tf.float32, [None])

        kernel_num = [16, 16]
        hidden_size = [128 - 16]
        emb_size = 16

        if True:
            conv1 = tf.layers.conv2d(input_view,
                                     filters=kernel_num[0], kernel_size=3, activation=tf.nn.relu)
            conv2 = tf.layers.conv2d(conv1,
                                     filters=kernel_num[1], kernel_size=3, activation=tf.nn.relu)
            flatten_view = tf.reshape(conv2, [-1, np.prod([v.value for v in conv2.shape[1:]])])
        else:
            flatten_view = tf.reshape(input_view, [-1, np.prod(view_space)])
            flatten_view = tf.layers.dense(flatten_view, units=64, activation=tf.nn.relu)

        h_emb = tf.layers.dense(input_feature, units=emb_size, activation=tf.nn.relu)

        all_flat = tf.concat((flatten_view, h_emb), axis=1)
        dense = tf.layers.dense(all_flat, units=hidden_size[0], use_bias=False, activation=tf.nn.relu)

        policy = tf.layers.dense(dense, units=self.num_actions, use_bias=False, activation=tf.nn.softmax)
        policy = tf.clip_by_value(policy, 1e-6, 1 - 1e-6)
        value = tf.layers.dense(dense, units=1)

        action_mask = tf.one_hot(action, self.num_actions)

        prob = tf.reduce_sum(policy * action_mask, axis=1)
        pg_loss = tf.reduce_mean(-advantage * tf.log(prob))
        vf_loss = self.state_coef * tf.reduce_mean((reward - value) ** 2)
        neg_entropy = self.ent_coef * tf.reduce_mean(tf.reduce_sum(policy * tf.log(policy), axis=1))
        total_loss = pg_loss + vf_loss + neg_entropy

        train_op = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(total_loss)

        self.input_view = input_view
        self.input_feature = input_feature
        self.advantage = advantage
        self.reward     = reward
        self.action     = action

        self.policy, self.value = policy, value
        self.train_op = train_op
        self.actor_loss, self.critic_loss, self.reg_loss = pg_loss, vf_loss, neg_entropy
        self.total_loss = total_loss

    def infer_action(self, raw_obs, ids, *args, **kwargs):
        view, feature = raw_obs[0], raw_obs[1]
        n = len(view)

        policy = self.sess.run(self.policy, {self.input_view: view,
                                             self.input_feature: feature})
        actions = np.arange(self.num_actions)

        ret = np.empty(n, dtype=np.int32)
        for i in range(n):
            ret[i] = np.random.choice(actions, p=policy[i])

        return ret

    def train(self, sample_buffers, print_every=1000):
        # deal with buffer type
        if isinstance(sample_buffers, EpisodesBuffersQueue):  # a queue of buffer
            buffers = sample_buffers.get_buffers()
        elif isinstance(sample_buffers, EpisodesBuffer):  # a single buffer
            buffers = [sample_buffers]  # for consistent, make it iterable
        else:
            raise RuntimeError("unsupported buffer type")

        n = 0
        for sample_buffer in buffers:
            for episode in sample_buffer.get_dict_buf().values():
                if episode.terminal:
                    n += len(episode.rewards)
                else:
                    n += len(episode.rewards) - 1

        # resize to the new size
        self.view_buf.resize((n,) + self.view_space)
        self.feature_buf.resize((n,) + self.feature_space)
        self.action_buf.resize(n)
        self.reward_buf.resize(n)
        self.value_buf.resize(n)
        self.advantage_buf.resize(n)
        view, feature = self.view_buf, self.feature_buf
        action, value = self.action_buf, self.value_buf
        reward, advantage = self.reward_buf, self.advantage_buf

        ct = 0
        gamma = self.reward_decay
        # collect episodes from multiple separate buffers to a continuous buffer
        for sample_buffer in buffers:
            for id, episode in sample_buffer.get_dict_buf().iteritems():
                v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards

                m = len(episode.rewards)
                value = self.sess.run(self.value, feed_dict={
                    self.input_view: v,
                    self.input_feature: f,
                })
                value = value.reshape((len(value),))

                delta_t = np.empty(m)
                if episode.terminal:
                    r = np.array(r)
                    delta_t[:m-1] = r[:m-1] + gamma * value[1:m] - value[:m-1]
                    delta_t[m-1]  = r[m-1]  + gamma * 0          - value[m-1]
                else:
                    delta_t[:m-1] = r[:m-1] + gamma * value[1:m] - value[:m-1]
                    m -= 1
                    v, f, a = v[:-1], f[:-1], a[:-1]

                # discount advantage
                keep = 0
                for i in reversed(range(m)):
                    keep = keep * gamma + delta_t[i]
                    advantage[ct+i] = keep

                view[ct:ct+m] = v
                feature[ct:ct+m] = f
                action[ct:ct+m]  = a
                reward[ct:ct+m] = advantage[ct:ct+m] + value[:m]
                ct += m
        assert n == ct

        if n == 0:
            return [0, 0, 0], 0

        _, a_loss, c_loss, r_loss, state_value = self.sess.run([self.train_op, self.actor_loss, self.critic_loss,
                                                                self.reg_loss, self.value], feed_dict={
            self.input_view:    view,
            self.input_feature: feature,
            self.action:        action,
            self.advantage:     advantage,
            self.reward:        reward,
        })
        mean_value = np.mean(state_value)

        return [a_loss, c_loss, r_loss], mean_value

    def save(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.save(self.sess, os.path.join(dir_name, "tfa2c_%d" % epoch))

    def load(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.restore(self.sess, os.path.join(dir_name, "tfa2c_%d" % epoch))

    def get_info(self):
        return "a2c train_time: %d" % (self.train_ct)
