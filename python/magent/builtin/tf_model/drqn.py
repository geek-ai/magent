import time

import numpy as np
import tensorflow as tf
import os

from magent.model import BaseModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue


class DeepRecurrentQNetwork(BaseModel):
    def __init__(self, env, handle, name,
                 batch_size=8, train_length=16, reward_decay=0.99, learning_rate=1e-4,
                 train_freq=1, target_freq=2000, eval_obs=None,
                 use_dueling=True, use_double=False, use_episode_train=False):
        BaseModel.__init__(self, env, handle)
        # ======================== set config  ========================
        self.env = env
        self.handle = handle
        self.name = name
        self.view_space = env.get_view_space(handle)
        self.feature_space = env.get_feature_space(handle)
        self.num_actions = env.get_action_space(handle)[0]

        self.batch_size   = batch_size
        self.handle = handle
        self.name = name
        self.train_length = train_length
        self.learning_rate= learning_rate
        self.train_freq   = train_freq   # train time of every sample (s,a,r,s')
        self.target_freq  = target_freq  # update frequency of target network
        self.eval_obs = eval_obs

        self.use_dueling  = use_dueling
        self.use_double   = use_double
        self.use_episode_train = use_episode_train
        self.skip_error = 4
        self.pad_before_len = train_length - 1

        self.agent_states = {}
        self.train_ct = 0

        # ======================= build network =======================
        # input place holder
        self.target = tf.placeholder(tf.float32, [None])

        self.input_view    = tf.placeholder(tf.float32, (None,) + self.view_space)
        self.input_feature = tf.placeholder(tf.float32, (None,) + self.feature_space)
        self.action = tf.placeholder(tf.int32, [None])

        self.batch_size_ph   = tf.placeholder(tf.int32, [])
        self.train_length_ph = tf.placeholder(tf.int32, [])

        # build graph
        with tf.variable_scope(self.name):
            with tf.variable_scope("eval_net_scope"):
                self.eval_scope_name   = tf.get_variable_scope().name
                self.qvalues, self.state_in, self.rnn_state = \
                    self._create_network(self.input_view, self.input_feature)

            with tf.variable_scope("target_net_scope"):
                self.target_scope_name = tf.get_variable_scope().name
                self.target_qvalues, self.target_state_in, self.target_rnn_state = \
                    self._create_network(self.input_view, self.input_feature)

        # train op
        self.gamma = reward_decay
        self.actions_onehot = tf.one_hot(self.action, self.num_actions)
        self.td_error = tf.square(
                self.target - tf.reduce_sum(tf.multiply(self.actions_onehot, self.qvalues), axis=1)
            )
        if self.use_episode_train:
            self.loss = tf.reduce_mean(self.td_error)
        else:
            self.mask_0 = tf.zeros(shape=[self.batch_size_ph, self.skip_error])
            self.mask_1 = tf.ones(shape=[self.batch_size_ph, self.train_length_ph - self.skip_error])
            self.mask = tf.concat([self.mask_0, self.mask_1], axis=1)
            self.mask = tf.reshape(self.mask, [-1])
            self.loss = tf.reduce_mean(self.td_error * self.mask)

        self.train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)

        # target network update op
        self.update_target_op = []
        t_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.target_scope_name)
        e_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.eval_scope_name)
        for i in range(len(t_params)):
            self.update_target_op.append(tf.assign(t_params[i], e_params[i]))

        # init tensorflow session
        config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.sess.run(tf.global_variables_initializer())

        # init training buffers
        self.view_buf = np.empty((1,) + self.view_space)
        self.feature_buf = np.empty((1,) + self.feature_space)
        self.action_buf, self.reward_buf = np.empty(1, dtype=np.int32), np.empty(1)
        self.terminal_buf = np.empty(1, dtype=np.bool)

    def _create_network(self, input_view, input_feature):
        kernel_num  = [32, 32]
        emb_size    = 32
        hidden_size = [256 - emb_size]

        h_conv1 = tf.layers.conv2d(input_view, filters=kernel_num[0], kernel_size=3, activation=tf.nn.relu)
        h_conv2 = tf.layers.conv2d(h_conv1, filters=kernel_num[1], kernel_size=3, activation=tf.nn.relu)
        flatten_view = tf.reshape(h_conv2, [-1, np.prod([v.value for v in h_conv2.shape[1:]])])

        h_emb = tf.layers.dense(input_feature, units=emb_size, activation=tf.nn.relu)

        all_flat = tf.concat((flatten_view, h_emb), axis=1)
        dense = tf.layers.dense(all_flat, units=hidden_size[0], activation=tf.nn.relu)

        # RNN
        state_size = hidden_size[0]
        rnn_cell = tf.contrib.rnn.GRUCell(num_units=state_size)

        rnn_in = tf.reshape(dense, shape=[self.batch_size_ph, self.train_length_ph, state_size])
        state_in = rnn_cell.zero_state(self.batch_size_ph, tf.float32)
        rnn, rnn_state = tf.nn.dynamic_rnn(
            cell=rnn_cell, inputs=rnn_in, dtype=tf.float32, initial_state=state_in
        )
        rnn = tf.reshape(rnn, shape=[-1, state_size])

        if self.use_dueling:
            value = tf.layers.dense(rnn, units=1)
            advantage = tf.layers.dense(rnn, units=self.num_actions, use_bias=False)

            qvalues = value + advantage - tf.reduce_mean(advantage, axis=1, keep_dims=True)
        else:
            qvalues = tf.layers.dense(rnn, units=self.num_actions)

        self.state_size = state_size
        return qvalues, state_in, rnn_state

    def _get_agent_states(self, ids):
        n = len(ids)
        states = np.empty([n, self.state_size])
        default = np.zeros([self.state_size])
        for i in range(n):
            states[i] = self.agent_states.get(ids[i], default)
        return states

    def _set_agent_states(self, ids, states):
        if len(ids) <= len(self.agent_states) * 0.5:
            self.agent_states = {}
        for i in range(len(ids)):
            self.agent_states[ids[i]] = states[i]

    def infer_action(self, raw_obs, ids, policy='e_greedy', eps=0):
        view, feature = raw_obs[0], raw_obs[1]
        n = len(ids)

        states = self._get_agent_states(ids)
        qvalues, states = self.sess.run([self.qvalues, self.rnn_state], feed_dict={
            self.input_view:    view,
            self.input_feature: feature,
            self.state_in:      states,
            self.batch_size_ph: n,
            self.train_length_ph: 1
        })
        self._set_agent_states(ids, states)
        best_actions = np.argmax(qvalues, axis=1)

        if policy == 'e_greedy':
            random = np.random.randint(self.num_actions, size=(n,))
            cond = np.random.uniform(0, 1, size=(n,)) < eps
            ret = np.where(cond, random, best_actions)
        elif policy == 'greedy':
            ret = best_actions

        return ret.astype(np.int32)

    def _calc_target(self, next_view, next_feature, rewards, terminal, batch_size, train_length):
        n = len(rewards)
        if self.use_double:
            t_qvalues, qvalues = self.sess.run([self.target_qvalues, self.qvalues], feed_dict={
                self.input_view:    next_view,
                self.input_feature: next_feature,
                self.batch_size_ph: batch_size,
                self.train_length_ph: train_length})
            next_value = t_qvalues[np.arange(n), np.argmax(qvalues, axis=1)]
        else:
            t_qvalues = self.sess.run(self.target_qvalues, feed_dict={
                self.input_view:      next_view,
                self.input_feature:   next_feature,
                self.batch_size_ph:   batch_size,
                self.train_length_ph: train_length})
            next_value = np.max(t_qvalues, axis=1)

        target = np.where(terminal, rewards, rewards + self.gamma * next_value)

        return target

    def train_by_episode(self, sample_buffers, print_every=1000):
        total_loss = 0

        # deal with buffer type
        if isinstance(sample_buffers, EpisodesBuffersQueue):  # a queue of buffer
            buffers = sample_buffers.get_buffers()
        elif isinstance(sample_buffers, EpisodesBuffer):  # a single buffer
            buffers = [sample_buffers]  # for consistent, make it iterable
        else:
            raise RuntimeError("unsupported buffer type")

        start_time = time.time()
        mean_qvalue = 0
        ct = 0

        max_len = 1000

        view, feature = np.empty((max_len,) + self.view_space), np.empty((max_len,) + self.feature_space)
        action, reward = np.empty(max_len, dtype=np.int32), np.empty(max_len)
        terminal = np.empty(max_len, dtype=np.bool)

        for sample_buffer in buffers:
            for episode in sample_buffer.get_dict_buf().values():
                v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards
                m = len(v)

                view[:m], feature[:m] = v, f
                action[:m], reward[:m] = a, r
                terminal[:m] = False
                view[m], feature[m] = v[-1], f[-1]  # padding last frame

                if episode.terminal:
                    terminal[m-1] = True

                target = self._calc_target(view[1:m+1], feature[1:m+1], reward[:m], terminal[:m], 1, m)
                _, loss = self.sess.run([self.train_op, self.loss], feed_dict={
                    self.input_view:    view[:m],
                    self.input_feature: feature[:m],
                    self.action:        action[:m],
                    self.target:        target[:m],
                    self.train_length_ph: m,
                    self.batch_size_ph:   1
                })
                total_loss += loss

                if self.train_ct > self.target_freq:
                    self.sess.run(self.update_target_op)
                    self.train_ct = 0

                mean_qvalue = 0.98 * mean_qvalue + np.mean(target) * 0.02
                if ct % print_every == 0:
                    print("number %5d, loss %.6f, qvalue %.6f" % (ct, loss, mean_qvalue))

                ct += 1
                self.train_ct += 1.0 * m / self.batch_size

        total_time = time.time() - start_time
        step_average = total_time / max(1.0, (ct / 1000.0))
        print("batches: %d,  total time: %.2f,  1k average: %.2f" % (ct, total_time, step_average))

        return total_loss / ct if ct != 0 else 0, mean_qvalue

    def train(self, sample_buffers, print_every=1000):
        if self.use_episode_train:
            return self.train_by_episode(sample_buffers, print_every)

        batch_size = self.batch_size
        train_length = self.train_length
        pad_before_len = self.pad_before_len
        total_loss = 0

        # deal with buffer type
        if isinstance(sample_buffers, EpisodesBuffersQueue):  # a queue of buffer
            buffers = sample_buffers.get_buffers()
        elif isinstance(sample_buffers, EpisodesBuffer):  # a single buffer
            buffers = [sample_buffers]  # for consistent, make it iterable
        else:
            raise RuntimeError("unsupported buffer type")

        # calc buffer size
        n = n_head = 0
        pad = 1  # 1 is for padding the last frame
        for sample_buffer in buffers:
            for episode in sample_buffer.get_dict_buf().values():
                n += len(episode.rewards)
                pad += pad_before_len
                n_head += pad_before_len + len(episode.rewards) - (train_length - 1)
                if not episode.terminal:
                    n_head -= 1

        # resize to the new size
        self.view_buf.resize((n + pad,) + self.view_space)
        self.feature_buf.resize((n + pad,) + self.feature_space)
        self.action_buf.resize(n + pad)
        self.reward_buf.resize(n + pad)
        self.terminal_buf.resize(n + pad)
        view, feature = self.view_buf, self.feature_buf
        action, reward = self.action_buf, self.reward_buf
        terminal = self.terminal_buf

        head = np.empty(n_head, dtype=np.int)
        ct = p_head = 0

        # collect episodes from multiple separate buffers to a continuous buffer
        for sample_buffer in buffers:
            for id, episode in sample_buffer.get_dict_buf().items():
                v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards
                m = len(episode.rewards)

                # pad before
                if pad_before_len != 0:
                    view[ct:ct + pad_before_len] = np.zeros_like(view[0:pad_before_len])
                    feature[ct:ct + pad_before_len] = np.zeros_like(feature[0:pad_before_len])
                    action[ct:ct + pad_before_len] = np.zeros_like(action[0:pad_before_len])
                    reward[ct:ct + pad_before_len] = np.zeros_like(reward[0:pad_before_len])
                    terminal[ct:ct + pad_before_len] = np.ones_like(terminal[0:pad_before_len])

                ct_beg = ct + pad_before_len
                view[ct_beg:ct_beg+m] = v
                feature[ct_beg:ct_beg+m] = f
                action[ct_beg:ct_beg+m]  = a
                reward[ct_beg:ct_beg+m]  = r
                terminal[ct_beg:ct_beg+m] = False

                if episode.terminal:
                    add_head = m + pad_before_len - (train_length - 1)
                    terminal[ct_beg+m-1] = True
                else:  # if it is not terminal, then the last frame is only for calculating target, not for training
                    add_head = m + pad_before_len - (train_length - 1) - 1

                head[p_head:p_head+add_head] = np.arange(ct, ct + add_head)
                p_head += add_head
                ct += m + pad_before_len

        assert ct == n + pad - 1 and p_head == n_head

        # for shuffle
        np.random.shuffle(head)
        pick = np.empty(batch_size * train_length, dtype=np.int32)

        # train batches
        n_batches = int(self.train_freq * n / (batch_size * (train_length - self.skip_error)))
        print("batch number %d" % n_batches)

        ct = pt = 0
        mean_qvalue = 0
        start_time = time.time()
        for i in range(n_batches):
            head_batch = head[pt:pt + batch_size]
            for j, start in enumerate(head_batch):
                pick[j * train_length:(j+1) * train_length] = np.arange(start, start + train_length)

            assert max(pick)+1 < len(view)
            # collect trajectories from different IDs to a single buffer
            target = self._calc_target(view[pick+1], feature[pick+1],
                                       reward[pick], terminal[pick], batch_size, self.train_length)
            _, loss = self.sess.run([self.train_op, self.loss], feed_dict={
                self.input_view:      view[pick],
                self.input_feature:   feature[pick],
                self.action:          action[pick],
                self.target:          target,
                self.train_length_ph: self.train_length,
                self.batch_size_ph:   batch_size
            })
            total_loss += loss

            if self.train_ct % self.target_freq == 0:
                self.sess.run(self.update_target_op)

            mean_qvalue = 0.98 * mean_qvalue + np.mean(target) * 0.02
            if ct % print_every == 0:
                print("batch %5d, loss %.6f, qvalue %.6f" % (ct, loss, self._eval(target)))

            ct += 1
            pt = (pt + batch_size) % n_head
            self.train_ct += 1

        total_time = time.time() - start_time
        step_average = total_time / max(1.0, (ct / 1000.0))
        print("batches: %d,  total time: %.2f,  1k average: %.2f" % (ct, total_time, step_average))

        return total_loss / ct if ct != 0 else 0, self._eval(target)

    def _eval(self, target):
        if self.eval_obs is None:
            return np.mean(target)
        else:
            return np.mean(self.sess.run(self.target_qvalues, feed_dict={
                self.input_view:      self.eval_obs[0],
                self.input_feature:   self.eval_obs[1],
                self.batch_size_ph:   self.eval_obs[0].shape[0],
                self.train_length_ph: 1
            }))

    def save(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.save(self.sess, os.path.join(dir_name, "tfdrqn_%d" % epoch))

    def load(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.restore(self.sess, os.path.join(dir_name, "tfdrqn_%d" % epoch))

    def get_info(self):
        return "drqn train_time: %d" % (self.train_ct)
