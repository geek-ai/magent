import time
import os

import numpy as np
import tensorflow as tf

from magent.model import BaseModel
from magent.utility import EpisodesBuffer, EpisodesBuffersQueue


class DeepQNetwork(BaseModel):
    def __init__(self, env, handle, name,
                 batch_size=64, reward_decay=0.99, learning_rate=1e-4,
                 train_freq=1, target_freq=2000, eval_obs=None,
                 use_dueling=True, use_double=True, use_prior_sample=False):
        BaseModel.__init__(self, env, handle)
        # ======================== set config  ========================
        self.env = env
        self.handle = handle
        self.name = name
        self.view_space = env.get_view_space(handle)
        self.feature_space = env.get_feature_space(handle)
        self.num_actions  = env.get_action_space(handle)[0]

        self.batch_size   = batch_size
        self.learning_rate= learning_rate
        self.train_freq   = train_freq   # train time of every sample (s,a,r,s')
        self.target_freq  = target_freq  # update frequency of target network
        self.eval_obs     = eval_obs

        self.use_dueling  = use_dueling
        self.use_double   = use_double
        self.use_prior_sample = use_prior_sample

        self.train_ct = 0

        # ======================= build network =======================
        # input place holder
        self.target = tf.placeholder(tf.float32, [None])
        self.weight = tf.placeholder(tf.float32, [None])

        self.input_view    = tf.placeholder(tf.float32, (None,) + self.view_space)
        self.input_feature = tf.placeholder(tf.float32, (None,) + self.feature_space)
        self.action = tf.placeholder(tf.int32, [None])

        # build graph
        with tf.variable_scope(self.name):
            with tf.variable_scope("eval_net_scope"):
                self.eval_scope_name   = tf.get_variable_scope().name
                self.qvalues = self._create_network(self.input_view, self.input_feature)

            with tf.variable_scope("target_net_scope"):
                self.target_scope_name = tf.get_variable_scope().name
                self.target_qvalues = self._create_network(self.input_view, self.input_feature)

        # train op
        self.gamma = reward_decay
        self.actions_onehot = tf.one_hot(self.action, self.num_actions)
        td_error = tf.square(self.target - tf.reduce_sum(tf.multiply(self.actions_onehot, self.qvalues), axis=1))
        self.loss = tf.reduce_mean(td_error)

        self.train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.loss)

        # output action
        best_action = tf.argmax(self.qvalues, axis=1)
        best_action = tf.to_int32(best_action)
        self.eps = tf.placeholder(tf.float32)
        random_action = tf.random_uniform(tf.shape(best_action), 0, self.num_actions, tf.int32)
        should_explore = tf.random_uniform(tf.shape(best_action), 0, 1) < self.eps
        self.output_action = tf.where(should_explore,
                                      random_action,
                                      best_action)

        # target network update op
        self.update_target_op = []
        t_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.target_scope_name)
        e_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.eval_scope_name)
        for i in range(len(t_params)):
            self.update_target_op.append(tf.assign(t_params[i], e_params[i]))

        # init tensorflow session
        config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.sess.run(tf.global_variables_initializer())

        # init training buffers
        self.view_buf = np.empty((1,) + self.view_space)
        self.feature_buf = np.empty((1,) + self.feature_space)
        self.action_buf, self.reward_buf = np.empty(1, dtype=np.int32), np.empty(1)
        self.terminal_buf = np.empty(1, dtype=np.bool)
        self.weight_buf = np.empty(1)

    def _create_network(self, input_view, input_feature):
        kernel_num  = [32, 32]
        emb_size    = 32
        hidden_size = [256 - emb_size]

        h_conv1 = tf.layers.conv2d(input_view, filters=kernel_num[0], kernel_size=3, activation=tf.nn.relu)
        h_conv2 = tf.layers.conv2d(h_conv1, filters=kernel_num[1], kernel_size=3, activation=tf.nn.relu)
        flatten_view = tf.reshape(h_conv2, [-1, np.prod([v.value for v in h_conv2.shape[1:]])])

        h_emb = tf.layers.dense(input_feature, units=emb_size, activation=tf.nn.relu)

        all_flat = tf.concat((flatten_view, h_emb), axis=1)
        dense = tf.layers.dense(all_flat, units=hidden_size[0], activation=tf.nn.relu)

        if self.use_dueling:
            value = tf.layers.dense(dense, units=1)
            advantage = tf.layers.dense(dense, units=self.num_actions, use_bias=False)

            qvalues = value + advantage - tf.reduce_mean(advantage, axis=1, keep_dims=True)
        else:
            qvalues = tf.layers.dense(dense, units=self.num_actions)

        return qvalues

    def infer_action(self, raw_obs, ids, policy='e_greedy', eps=0):
        view, feature = raw_obs[0], raw_obs[1]

        if policy == 'e_greedy':
            eps = eps
        elif policy == 'greedy':
            eps = 0

        ret = self.sess.run(self.output_action, feed_dict={
            self.input_view: view,
            self.input_feature: feature,
            self.eps: eps})

        return ret

    def _calc_target(self, next_view, next_feature, rewards, terminal):
        n = len(rewards)
        if self.use_double:
            t_qvalues, qvalues = self.sess.run([self.target_qvalues, self.qvalues],
                                               feed_dict={self.input_view: next_view,
                                                          self.input_feature: next_feature})
            next_value = t_qvalues[np.arange(n), np.argmax(qvalues, axis=1)]
        else:
            t_qvalues = self.sess.run(self.target_qvalues, {self.input_view: next_view,
                                                            self.input_feature: next_feature})
            next_value = np.max(t_qvalues, axis=1)

        target = np.where(terminal, rewards, rewards + self.gamma * next_value)

        return target

    def train(self, sample_buffers, print_every=1000):
        batch_size = self.batch_size
        total_loss = 0

        # deal with buffer type
        if isinstance(sample_buffers, EpisodesBuffersQueue):  # a queue of buffer
            buffers = sample_buffers.get_buffers()
        elif isinstance(sample_buffers, EpisodesBuffer):  # a single buffer
            buffers = [sample_buffers]  # for consistent, make it iterable
        else:
            raise RuntimeError("unsupported buffer type")

        # calc buffer size
        n = non_terminal = 0
        pad = 1  # 1 is for padding the last frame
        max_reward, min_reward = - 1 << 30, 1 << 30
        for sample_buffer in buffers:
            for episode in sample_buffer.get_dict_buf().values():
                n += len(episode.rewards)
                r = sum(episode.rewards)
                max_reward = max(max_reward, r)
                min_reward = min(min_reward, r)
                if not episode.terminal:
                    non_terminal += 1
        if max_reward == min_reward:
            min_reward -= 1
        # print("accumulated reward : max %.2f, min %.2f" % (max_reward, min_reward))

        # resize to the new size
        self.view_buf.resize((n + pad,) + self.view_space)
        self.feature_buf.resize((n + pad,) + self.feature_space)
        self.action_buf.resize(n + pad)
        self.reward_buf.resize(n + pad)
        self.terminal_buf.resize(n + pad)
        # self.weight_buf.resize(n + pad)
        view, feature = self.view_buf, self.feature_buf
        action, reward = self.action_buf, self.reward_buf
        terminal = self.terminal_buf
        # weight = self.weight_buf

        head = np.empty(n - non_terminal, dtype=np.int)
        ct = p_head = 0

        # collect episodes from multiple separate buffers to a continuous buffer
        for sample_buffer in buffers:
            for id, episode in sample_buffer.get_dict_buf().items():
                v, f, a, r = episode.views, episode.features, episode.actions, episode.rewards
                m = len(episode.rewards)

                view[ct:ct+m] = v
                feature[ct:ct+m] = f
                action[ct:ct+m]  = a
                reward[ct:ct+m]  = r
                terminal[ct:ct+m] = False
                # weight[ct:ct+m] = (sum(r) - min_reward) / (max_reward - min_reward) * 2

                if episode.terminal:
                    head[p_head:p_head+m] = np.arange(ct, ct + m)
                    p_head += m
                    terminal[ct+m-1] = True
                else:  # if it is not terminal, then the last frame is only for calculating target, not for training
                    head[p_head:p_head+m-1] = np.arange(ct, ct + m - 1)
                    p_head += m-1

                ct += m
        assert ct == n + pad - 1 and p_head == n - non_terminal

        # for shuffle
        np.random.shuffle(head)

        # train batches
        n_batches = int(self.train_freq * n / batch_size)
        print("batch number %d" % n_batches)

        ct = pt = 0
        start_time = time.time()
        for i in range(n_batches):
            pick = head[pt:pt + batch_size]
            assert max(pick)+1 < len(view)
            target = self._calc_target(view[pick+1], feature[pick+1],
                                       reward[pick], terminal[pick])
            _, loss = self.sess.run([self.train_op, self.loss], feed_dict={
                self.input_view:    view[pick],
                self.input_feature: feature[pick],
                self.action:        action[pick],
                # self.weight:        weight[pick],
                self.target:        target,
            })
            total_loss += loss

            if self.train_ct % self.target_freq == 0:
                self.sess.run(self.update_target_op)

            if ct % print_every == 0:
                print("batch %5d,  loss %.6f, eval %.6f" % (ct, loss, self._eval(target)))

            ct += 1
            pt = (pt + batch_size) % (n - non_terminal)
            self.train_ct += 1

        total_time = time.time() - start_time
        step_average = total_time / max(1.0, (ct / 1000.0))
        print("batches: %d,  total time: %.2f,  1k average: %.2f" % (ct, total_time, step_average))

        return total_loss / ct if ct != 0 else 0, self._eval(target)

    def _eval(self, target):
        if self.eval_obs is None:
            return np.mean(target)
        else:
            return np.mean(self.sess.run([self.qvalues], feed_dict={
                self.input_view: self.eval_obs[0],
                self.input_feature: self.eval_obs[1]
            }))

    def save(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name, )
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.save(self.sess, os.path.join(dir_name, "tfdqn_%d" % epoch))

    def load(self, dir_name, epoch):
        dir_name = os.path.join(dir_name, self.name)
        model_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.name)
        saver = tf.train.Saver(model_vars)
        saver.restore(self.sess, os.path.join(dir_name, "tfdqn_%d" % epoch))

    def get_info(self):
        return "dqn train_time: %d" % (self.train_ct)
