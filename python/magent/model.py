class BaseModel:
    def __init__(self, env, handle, *args, **kwargs):
        pass

    def infer_action(self, *args, **kwargs):
        pass

    def train(self, *args, **kwargs):
        return 0, 0    # loss, mean value

    def get_info(self, *args, **kwargs):
        pass

    def save(self, *args, **kwargs):
        pass

    def load(self, *args, **kwargs):
        pass
